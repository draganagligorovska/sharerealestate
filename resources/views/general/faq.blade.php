@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header"><h5>{{ __('QUESTIONS  & ANSWERS') }}</h5></div>

                <div class="card-body">
                   <h6><b>What are the advantages of purchasing real-estate collectively?</b></h6>
                   <p>There can be numerous advantages:</p>
                   <p>Economically, larger properties tend to be much less expensive per square meter. Purchasing a much larger property with other people tends to reduce the price of the purchase on a “per room” basis, or a “per square meter” basis. Consider a 1 bedroom apartment for for 300,000 EUR, compared to a 6 bedroom chateau for 1,000,000 EUR. </p>
                   <p>Maintenance is always an issue with real-estate. If you pay a third party for maintenance services, it is generally far less expensive, per person, to share the cost of maintaining a large property than a single person maintaining a small property. </p>
                   <p>Socially, it is often more fun to live amongst a group of people. Whether your group is composed of 2, 3 or more purchasers, you will be able to join a purchase group composed of members who are compatible with your lifestyle and personal tastes.</p>
                   <p>Selling your property in collective real-estate will be far less expensive, and possibly easier than selling your own property individually. The largest costs of selling real-estate are the agent fees and legal fees for transfer of title. We strongly advise our members to always structure their project with a civil real-estate company. This permits each member to simply transfer their part of the company to the new beneficial owner. This eliminates cadastral fees, publicity fees. Also, if a member of a group wishes to sell his/her part of property, it is likely that the remaining owners will seek a purchaser within their social network.. After all, they will have to deal with the person who replaces the exiting member. Effectively, this means that the remaining members are act as an agent, often eliminating the need for a real-estate agent.</p>
                   <p>Luxury service also becomes accessible with shared real-estate. The cost of in-house cleaning and cooking services, in-house child care, and more, can all be obtained for a modest price per person in a collective arrangement.</p>
                   <p>Prolonged absence is often a source of risks for individual property owners. Leaving a property uninhabited for weeks or months increases risk of burglary, squatters, unseen damages (water leaks, fires ...etc). A collectively owned property is much less likely to be left uninhabited for any length of time.</p>
                   <p>Furniture and equipment cost can become extremely very low per person. Consider the cost, for a single person, of furnishing and fitting a kitchen, laundry room and common room. Even if a much larger refrigerator, laundry machines, stoves and ovens, and a massive couch is purchased, the cost per person in a collective arrangement is likely to be far less.</p>
                   <p>Multiple dwellings become more accessible in shared property arrangements. Think of the cost of a single bedroom apartment in a city and a small cottage in the country. For significantly less money, you may have be part of a collective large 4 bedroom condominium in the city, and maybe a collective 8 bedroom ski chalet in the Alps.</p>
                   <p>Discovering the world can become a lot more fun and economical. By definition, vacation properties are generally not inhabited very often. Therefore, in a collective property arrangement, it is entirely possible to have 10 owners in a group purchase of a 3 bedroom vacation house in the Caribbean. The cost per person becomes extremely affordable. In fact, at such a low cost per person, why not also save up for a second vacation spot in the French countryside? Imagine all the new friends you can share vacations with in beautiful and exotic places! </p>

                   <h6><b>I am not sure about buying a property with strangers. What are the risks of conflict of personalities?</b></h6>
                   <p>Conflict of personalities is a concern for everyone. However, consider buying a smaller apartment or condominium individually, where you are effectively buying collectively the common parts of the complex and you don’t get to chose your neighbours. Purchasing a much larger property tends to reduce, or even exclude risks of “pesky neighbors” and lets you pre-screen and select who you want to share your property with. Read the group rules before joining a purchase group.</p>

                   <h6><b>How will my group and I decide how to manage the property and our relationships as owners?</b></h6>
                   <p>We strongly suggest that every collective purchase is structured with a civil real-estate company (or similar in various jurisdictions). A civil real-estate company is created for the purpose of purchasing the property. Each individual purchaser becomes a part owner of the the civil company. The constitution of the civil company governs all aspects of the relationship between each purchaser and the property. Maintenance costs, property taxes, transfer of property, evictions, intended use of property, resolution of conflicts … all may be pre-determined. Read the group rules before joining a purchase group.</p>

                   <h6><b>What happens if someone in my group sells their ownership to an undesirable person? </b></h6>
                   <p>Just like your neighbour selling to a new “pesky neighbor”, this is a rsk. However, this risk can be mitigated through a number of mechanisms in the civil real-estate company’s constitution. For example, the civil company’s constitution may foresee a vetting process whereby the remaining owners must accept the new purchaser of the exiting group members property, and the remaining owners may even have a “right of first refusal” (first dibs) on the exiting person’s ownership. This is essentially a balance between each person’s right to sell their part of the property, and the interests of the remaining owners. A civil real-estate company permits complete flexibility between these two interests. Read the group rules before joining a purchase group.</p>

                   <h6><b>Can I sublet my room in a collective property? </b></h6>
                   <p>This question is entirely dependant upon the rules in the constitution of your group’s civil real-estate company’s constitution. Read the group rules before joining a purchase group.</p>

                   <h6><b>Who gets the biggest bedroom? Who gets the small bedroom?</b></h6>
                   <p>Normally, properties intended to be used as the main resident for the purchase group will be based upon “persons per room pricing”. For example, a purchase group may determine that a 6 bedroom house, containing 1 big master bedroom, 4 medium size rooms, and 1 small room, is best suited for 3 couples, leaving 3 bedrooms rooms as guest rooms or office space. The group may determine that each couple will pay a premium for occupying the bigger room, and a discount for occupying the smaller rooms. Read the group rules before joining a purchase group.</p>

                   <h6><b>If I join a purchase group of 10 people buying a 3 bedroom ski chalet, who gets to go for Christmas, and who cannot go ? </b></h6>
                   <p>Because properties designated as “vacation” properties can generally be bought by far more people than there are bedrooms (there is an assumption everybody is not on vacation at the same time), this can create a need for “high season/low season” management. The group rules may allocate “peak season” to its group members on a rotational basis. Or, the group rules may foresee an auction in overbooking situations whereby the members must pay other members for the use in peak season in order to “win” the right to use the ski chalet during Christmas. Several elaborate rules may be adopted in order to overcome the difficulties involved in the use of vacation properties in peak season. Read the group rules before joining a purchase group.</p>

                   <h6><b>I want to create a purchase group, but I (don’t) want to share a place with people of a certain religion/race/nationality/sexual orientation ...etc. Can I discriminate in this manner ?</b></h6>
                   <p>When it comes to sharing your personal residential space with other people, you are entirely free to choose who you want to, or don’t want to live with. In order to restrict access to your purchase group to a certain profiles of people, we suggest that you be honest, yet polite. For example, instead of stating “[name of religion] not welcome”, consider “[other religion] only, please”, or “non overtly religious people only, please”. Understandably, discrimination can be a touchy subject, but when it comes to your personal living space, we believe you should be entirely free to determine who you choose to live with, or not to live with, even at the expense of momentarily hurt feelings or others. </p>

                   <h6><b>Is this service susceptible to scams ?</b></h6>
                   <p>Strictly speaking, NO, our service is not susceptible to scams. Our service is merely a matching and organisational platform. Our service is not directly involved in any real-estate transactions, and therefore not susceptible to any scams. Real-estate transactions are possible sources of scams, but this holds true whether or not you are using our service, or just buying real-estate individually in the traditional manner. To eliminate, or at least mitigate this risk to a minimum, we have vetted professional service providers who will assist in the sales of real-estate and legal work, thus ensuring maximum financial and legal security. </p>

                   <h6><b>Can I leave a group after applying and being accepted?</b></h6>
                   <p>Yes, of course. You may leave the group for any reason. In fact, you don’t even have to give a reason. However, fees for joining a group are non refundable.</p>

                   <h6><b>Why do you charge a fee for joining a group, in addition to a fee for becoming a member?</b></h6>
                   <p>A fee is charged for joining each group in order to incite sincerity of our members following through with the purchase transaction. We want to deter people from joining multiple groups, and then not following through when the group is ready to purchase the property, thus leaving the group without its entire means to complete its transaction. Otherwise said, the larger fee for joining a group should incentivise members to be serious about joining a group and actually following through with the purchase of the property.</p>

                   <h6><b>Why must I submit a copy of my identification ? Who has access to my identity documents?</b></h6>
                   <p>As a matching platform for members wishing enter into valuable transactions with other people, we must ensure sincerity between members. We want to avoid the creation of fake member profiles. The electronic copies of your identity documents will not be shared with anyone else, not even the other members within your group. Of course, when your group is ready to proceed with the purchase of the property, each member of the group will be required (not by us) to exchange information between themselves as a matter of due diligence.</p>

                   <h6><b>I want to create a purchase group for a property in [name of country], but there are no property listings and no professional service providers on your website in [name of country]. </b></h6>
                   <p>Tell us about it! We will do our best to locate property listings and service providers in the country where you wish to implement your collective real-estate project. </p>

                   <h6><b>I want to create a purchase group for a yacht. Can I do this here ?</b></h6>
                   <p>Absolutely, yes! We welcome innovative collective purchases for all sorts of projects. If your project requires specific configurations, forms or service providers no already on our website, contact us and we will do our best to accommodate your collective purchase project.</p>

                   <h6><b>I want to create a purchase group to acquire a business. Can I do this here ?</b></h6>
                   <p>Absolutely, yes! We welcome innovative collective purchases for all sorts of projects. If your project requires specific configurations, forms or service providers no already on our website, contact us and we will do our best to accommodate your collective purchase project.</p>

                   <h6><b>I want to create a purchase group for a property in a country that requires that only  local citizens own property (ex; Vietnam), but I am not a citizen of that country. What can I do ?</b></h6>
                   <p>Contact us. In most cases, we will be able to suggest legal structures which enable your project to comply with national law of the jurisdiction in question.</p>

                   <h6><b>I need financing to participate in a purchase group. What financing options exist ?</b></h6>
                   <p>Purchasing real-estate collectively generally has the benefit of greatly reducing the purchase price per person. However, collective purchasing of real-estate can be more complex to finance. </p>
                   <p>Mortgage loans are unlikely. Financial institutions which lend to its clients for purchasing real-estate will hold a mortgage (title of property) on the property being financed. In the event of default of mortgage loan payments, the lending institution will “foreclose” the property, meaning evict the occupants and sell the real-estate in order to be reimbursed. Moreover, the lending institution will also generally require all of the beneficial owners of the real-estate (each group member) to contract as a debtor for the loan. It is highly unlikely that the other purchase group members will be willing to take these risks for you.</p>
                   <p>Personal loans are really the best option. It is entirely possible that your bank will lend you money for a portion of your quote part of of the acquisition price of the property. However, because the other members in your purchase group will probably not consent to a mortgage on the real-estate for your loan, your lender will not have recourse against the real-estate. Therefore, if you qualify for a personal loan, you will probably only receive financing for smaller part of your purchase price compared to financing via a mortgage loan. But keep in mind that you are probably already benefiting from a lower cost basis individually, by purchasing collectively.</p>
                   <p>We are constantly seeking new methods of financing collective purchases, and we have dedicated a web page to sources for personal loans.</p>
                
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
