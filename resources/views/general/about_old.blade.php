@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header"><h5>{{ __('About Us') }}</h5></div>

                <div class="card-body">
            	     <h6><b>MISSION STATEMENT</b></h6>
                   <p>Share Real Estate is based on the idea of <strong>“living beyond your dreams, without spending beyond your means”</strong>.</p>
                   <p>Our main focus is providing lifestyle services, tailored to you life dreams. In addition, there are also remarkable financial advantages of collective purchases.</p>
                   <p>Share Real Estate helps its members dream big and participate in collective purchases of real-estate beyond their dreams. We achieve this by leveraging economies of scale and organizing collective purchase projects.</p>
                   <p>Share Real Estate works with professional service providers worldwide who ensure the utmost legal and financial security for its members, from the time of purchase, throughout the duration of ownership, and at time of exiting ownership.</p>

                   <h6><b>WHO WE ARE - SENIOR MANAGEMENT</b></h6>
                   <p>Dragana Gligorovska Associate Director & Global IT Executive</p>
                   <p><img class="about_profile" src="{{ asset('images/dragana.jpg') }}" alt="Justin Oliver McCarthy"></p>
                   <p>Dragana is an accomplished IT engineer. Born in Skopje, Macedonia, Dragana attended the Faculty of Computer Science and Engineering. She has worked as a web developer and IT engineer on a number of global projects. Dragana is the developer of Share Real Estate’s IT platform and is a founding member of Share Real Estate and an executive of the board of directors.</p>
                   <p>Justin Oliver McCarthy Associate Director & Global Legal Executive</p>
                   <p><img class="about_profile" src="{{ asset('images/justin.jpg') }}" alt="Justin Oliver McCarthy"></p>
                   <p>Justin comes from a financial services background. Specialised in financial market law, he has served as legal counsel for various hedge funds, investment banks, brokerages and other financial institutions in France, England and Ireland, and has developed several global business projects over the course of his career. Justin introduced the shared real-estate concept and is a founding member of Share Real Estate and an executive of the board of directors.</p>
                
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
