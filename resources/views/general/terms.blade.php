@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header"><h5>{{ __('HOW OUR COLLECTIVE PURCHASE PLATFORM WORKS') }}</h5></div>

                <div class="card-body">
                   <h6><b>FOR PURCHASERS</b></h6>
                   <h7><b>Join an existing group of purchasers</b></h7>
                   <ol>
                    <li>Browse existing projects using numerous filters to determine location, profiles of group members, price, type of property, use of property… etc. </li>
                    <li>Become a member to create your personal profile page.</li>
                    <li>Submit your application to existing groups which you would like to join.</li>
                    <li>After application to an existing group, the group leader(s) will decide whether or not your profile is compatible with the group.</li>
                    <li>Once the group has reached its target size  (in terms of people or capital subscriptions), the group will be “closed” to new members. The group will then be directed to the appropriate service providers for the legal formalities and the sale transaction (legal professionals, notaries, real-estate agents)</li>
                    <li>If your group requires extra services (renovations, property maintenance … etc) we have that covered too.</li>
                   </ol>
                   <h7><b>Create a new group</b></h7>
                   <p>If you have a specific project in mind, or if you are seeking specific regions or groups which are not already in our database, starting your own purchase group may be preferable.</p>
                   <p>To create a new purchase group:</p>
                   <ol>
                    <li>Become a member to create your personal profile page.</li>
                    <li>Opt to “create a new purchase group”. This will provide you with all of the criteria filters in relation to your target property, the desired use of the property (residential, vacation, rental income ...etc), and the desired purchase group members. </li>
                    <li>Adopt your group rules. Use one of our templates, modify it to taste, or create your own.</li>
                    <li>When candidate group members apply to your group, you may accept or reject candidates. Of course, you may also contact them for an interview process prior to accepting or rejecting their application to your group.</li>
                    <li>Once the group has reached its target size  (in terms of people or capital subscriptions), the group will be “closed” to new members. The group will then be directed to the appropriate service providers for the legal formalities and the sale transaction (legal professionals, notaries, real-estate agents) </li>
                    <li>If your group requires extra services (renovations, property maintenance … etc) we have that covered too.</li>
                   </ol>
                   <h6><b>FOR VENDORS</b></h6>
                   <ol>
                     <li>Create a vendor page</li>
                     <li>Post one or more property listings</li>
                   </ol>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
