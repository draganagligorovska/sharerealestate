@extends('layouts.main')

@section('main_content')
<!-- Hero Section Begin -->
<section class="hero-section set-bg about-us" data-setbg="img/bg.jpg">
    <div class="container hero-text text-white">
        <h2>SPECIAL PURPOSE VEHICLES</h2>
    </div>
</section>
<!-- Hero Section End -->
<!-- Servies Section Begin -->
<section class="services-section">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="left-side">
                    <h2>SPV</h2>
                    <p>Special purpose vehicles (SPV) are legal entities (companies) which are formed for a very specific purpose, such as holding property ownership. </p>
                    <br>
                    <p>SPVs are particularly useful for collective purchases of real-estate, aircraft, and marine vessels.</p>
                    <br>
                    <p>The mechanism is fairly simple. Instead of the individuals in the group directly purchasing the property, they create an SPV and become shareholders of the SPV (proportionately to their contribution of the purchase price). Then, the SPV purchases the property in its own corporate name. </p>
                    <br>
                    <p>When a group of people wish to buy a property together, there are often many questions and concerns about how the property will be used, maintenance costs, one person selling his part of ownership, bad/reckless behaviour of a co-owner, succession of property due to death, taxes, insolvency and seizure, and the general administration of the property.</p>
                    <br>
                    <p>All of these topics can be neatly addressed and managed with an SPV. </p>
                    <br>
                    <p>Moreover, the use of an SPV for holding real-estate is also <u>extremely efficient for the purchase and sale of fractional property ownership</u>. If one of the persons decides to sell or donate his/her fractional ownership of the property, he/she simply sells his/her shares in the SPV. This often results in far less legal formalities and costs than selling/donating the property directly.</p>
                    <br>
                    <p>An SPV also offers greater legal security for the co-owners. One important example is the case of insolvency of a co-owner. An insolvent co-owner may have his/her property seized by an unsatisfied creditor. Without an SPV, this unsatisfied creditor would become a direct co-owner of the property after seizure. This can be the first step to disaster for the other co-owners. Effectively, in most jurisdictions, there is a legal principle according to which “nobody can be forced to maintain co-ownership”. This means that, in the absence of an SPV, the unsatisfied creditor can force the other co-owners to either buy out this creditor, or the creditor could force the sale of the entire property to receive the payment for the original debt. </p>
                    <br>
                    <p>If the co-owners incorporate an SPV is the ownership structure, the unsatisfied creditor could only seize the shares of its debtor, and attempt to sell these shares (not the property itself). The other shareholders of the SPV are under no obligation to purchase the shares from the unsatisfied creditor. This means that with an SPV, the co-owners are protected from undesirable consequences of insolvency of one or more other co-owners.</p>
                    <br>
                    <p>Favourable tax treatment is also another advantage of an SPV for owning property in many jurisdictions. This topic is far too vast and jurisdiction specific to explain here, but it is likely that there are important tax advantages by using an SPV in the ownership structure of fractional property.</p>
                    <br>
                    <p>When acquiring property with a group on Share-realestate.com, be sure to review the group’s rules and whether or not the group intends to use an SPV for the acquisition of property. </p>
                    <br>
                    <p>If you are creating a group on Share-realestate.com, we strongly advise you to consult a law firm, notary or corporate services provider to assist in setting up an SPV for your group. See our <a href="{{ url('/search_provider') }}">SERVICE PROVIDERS</a> to help guide you.</p>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Servies Section End -->
@endsection