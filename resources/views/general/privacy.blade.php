@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header"><h5>{{ __('OUR SERVICES') }}</h5></div>

                <div class="card-body">
                  <h6><b>What we do</b></h6>
                  <p>Share Real Estate is a platform designed to match persons (natural persons or legal entities) with other persons to form “purchase groups”. </p>
                  <p>Purchase groups are created at the initiative of our members, and composed of other like-minded members who decide to join a given purchase group. Our members may select from a vast array of criteria to determine which members and which purchase groups they with to associate with. </p>
                  <p>Our members to filter collective purchase projects in consideration of other members personal profile, as well as the profile of the collective purchase project.</p>
                  <p>We provide organisational infrastructure for our members, including modifiable “group rules” templates. Our “group rules” templates are designed to ensure a meeting of minds between members of a given purchase group. These templates include clauses governing group formation, leadership and group decision making, target properties, civil real-estate company formation, financing, property management, entry and exit of group members, and code of conduct. </p>
                  <p>We provide different “group rules” templates to suit different types of projects. For example, group rules pertaining to the collective purchase of “full time residential” properties to to focus greatly on personal profiles and collective living rules. Group rules pertaining to “Rental Investment” projects are more focussed on business and property management. </p>
                  <p>We supply our members and their purchase groups with a database of real-estate listings worldwide. We strive to increase this database on an ongoing basis. </p>
                  <p>Our members are also composed of the vendors, most of whom are real-estate agents. </p>
                  <p>We strive to provide our members with the means to create harmonious groups of purchasers, and align them with the most appropriate real-estate (and other goods) for their collective purchase projects. </p>
                  <h6><b>What we do NOT do</b></h6>
                  <p>We do <b>NOT</b> sell real estate. The vendors of real-estate are simply other members on our platform (usually professional real-estate agents, and some individual owners). We are not an estate agent.</p>
                  <p>We do <b>NOT</b> handle any money related to the purchase of real-estate. We only receive members’ fees for our matching and organisational services.</p>
                  <p>We do <b>NOT</b> benefit from any real-estate transactions. We are economically neutral between the purchasers and the vendors. </p>
                  <p>We do <b>NOT</b> negotiate on either party’s behalf. We maintain strict economic neutrality in our role.</p>
                  <p>We do <b>NOT</b> provide financing. However, we do provide contacts to our financing partners.</p>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
