@extends('layouts.main')

@section('main_content')
<!-- Hero Section Begin -->
<section class="hero-section set-bg about-us" data-setbg="img/bg.jpg">
    <div class="container hero-text text-white">
        <h2>About Us</h2>
    </div>
</section>
<!-- Hero Section End -->
<!-- About Us Sectiion Begin -->
<section class="about-us">
    <div class="container-fluid">
        <div class="row">
            <div class="col-xl-6 about-text-warp">
                <div class="about-text">
                    <h2>Mission Statement</h2>
                    <p>Share Realestate is based on the idea of <strong>“living beyond your dreams, without spending beyond your means”</strong>.</p>
                    <p>Our main focus is providing lifestyle opportunities, based on a shared economy model.</p>
                    <p>Share Realestate helps its members dream big and participate in collective projects beyond their individual means. This is achieved by organizing collective projects and leveraging economies of scale.</p>
                    <p>We work with professional service providers worldwide who ensure the utmost legal and financial security for its members, from the time of purchase, throughout the duration of ownership, and at the time of exiting a project.</p>
                </div>
            </div>
            <div class="col-xl-6">
                <div class="about-img">
                    <img src="img/about-img/1.jpg" alt="">
                    <img class="l-img" src="img/about-img/2.jpg" alt="">
                    <img class="r-img" src="img/about-img/3.jpg" alt="">
                </div>
            </div>
        </div>
    </div>
</section>
<!-- About Us Sectiion End -->
<!-- Servies Section Begin -->
<section class="services-section">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="left-side">
                    <h2>How it works</h2>
                    <p>Share Realestate is an online “shared economy” platform which permits its users to join or create groups of people who wish to participate in collective projects involving real-estate.</p> 
                    <br>
                    <p>We specialise in organising groups wishing to create or acquire:</p>
                    <br>
                    <p>- Residential property for co-living</p>
                    <p>- Office space for co-working</p>
                    <p>- Investment properties</p>
                    <p>- Vacation properties</p>
                    <p>- Other ventures involving realestate</p>
                    <br>
                    <p>With the exception of “investment properties”, most of our users will probably acquire a property for their own use and enjoyment, or as a place of business.</p>
                    <br>
                    <p>We provide our users with tools to filter profiles of people they wish to associate with in their collective project.</p> 
                    <br>
                    <p>We offer free templates for creating <a href="{{ url('/rules') }}">GROUP RULES</a>. These group rules are determined by the creator of the group, and are designed to inform members how the project will operate and various conditions of becoming a member of the group.</p>
                    <br>
                    <p>We collaborate with <a href="{{ url('/search_provider') }}">SERVICE PROVIDERS</a> (law firms, notaries, corporate service providers) to finalise the legal and financial aspects of the transaction.</p> 
                    <br>

                    <h2>What we do NOT do</h2>
                    <p>We do <strong>NOT</strong> sell real estate. The vendors of real-estate are simply other members on our platform (usually professional real-estate agents, and some individual owners). We are not an estate agent.</p>
                    <br>
                    <p>We do <strong>NOT</strong> handle any money related to the purchase of real-estate. We only receive members’ fees for our matching and organisational services.</p>
                    <br>
                    <p>We do <strong>NOT</strong> benefit from any real-estate transactions. We are economically neutral between the purchasers and the vendors.</p>
                    <br>
                    <p>We do <strong>NOT</strong> negotiate on either party’s behalf. We maintain strict economic neutrality in our role.</p>
                    <br>
                    <p>We do <strong>NOT</strong> provide financing. However, we do provide contacts to our financing partners.</p>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Servies Section End -->
<!-- Team Sectiion Begin -->
<section class="team-section">
    <div class="container">
        <div class="row">
            <div class="col-xl-12">
                <div class="section-title">
                    <h2><span>Who we are - Senior Management</span></h2>
                </div>
            </div>
        </div>
        <div class="row text-center">
            <div class="col-md-6">
                <div class="single-team">
                    <div class="membr-pic">
                        <img src="{{ asset('images/justin.jpg') }}" alt="Justin Oliver McCarthy">
                    </div>
                    <div class="membr-info">
                        <h2>Justin Oliver McCarthy</h2>
                        <p>Associate Director & Global Legal Executive</p>
                        <p>Justin comes from a financial services background. Specialised in financial market law, he has served as legal counsel for various hedge funds, investment banks, brokerages and other financial institutions in France, England and Ireland, and has developed several global business projects over the course of his career. Justin introduced the shared real-estate concept and is a founding member of Share Real Estate and an executive of the board of directors.</p>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="single-team">
                    <div class="membr-pic">
                        <img src="{{ asset('images/dragana.jpg') }}" alt="Dragana Gligorovska">
                    </div>
                    <div class="membr-info">
                        <h2>Dragana Gligorovska</h2>
                        <p>Associate Director & Software Engineer</p>
                        <p>Dragana is an accomplished IT engineer. Born in Skopje, Macedonia, Dragana attended the Faculty of Computer Science and Engineering. She has worked as a web developer and IT engineer on a number of global projects. Dragana is the developer of Share Real Estate’s IT platform and is a founding member of Share Real Estate and an executive of the board of directors.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Team Sectiion End -->
@endsection