@extends('layouts.main')

@section('main_content')
<script type="text/javascript">
    $( document ).ready(function() {
        $("#type").change(function(val){
            if($(this).val() == "vendors"){
                $text_vendor = 'Dear Madam, Sir\n\n';
                $text_vendor += 'We want to help you sell your real-estate.\n\n';
                $text_vendor += '> We have a database of international clients seeking to purchase high end/luxury real-estate worldwide\n';
                $text_vendor += '> We assemble collective purchase groups to purchase your real-estate\n';
                $text_vendor += '> We permit very large numbers of potential buyers to access your property listing\n';
                $text_vendor += '>  Our purchase groups are qualified leads, actively seeking to purchase\n\n';
                $text_vendor += 'Our Share Real Estate platform is like no other, and is geared to a global audience.\n\n';
                $text_vendor += 'Our unique proposition consists in assembling groups of purchasers, packaging them into a civil real-estate company, and then enabling this company to purchase your property.\n\n';
                $text_vendor += 'The result is many more potential purchasers for your property listing. Effectively, lower earners, assembled into a single civil real-estate company are now able to purchase your high end/luxury property. \n\n';
                $text_vendor += 'We will actively promote and put your property listing in front of all of our clients.\n\n';
                $text_vendor += 'ULTRA LOW FEES of, from only 1 Euro per month, per property listing ! \n\n';
                $text_vendor += 'Distribute your property listing now to our purchasers. It takes just minutes.\n\n';
                $text_vendor += 'Start now, here: \n';
                $text_vendor += 'www.share-realestate.com[AGENT/VENDOR/MEMBER LISTING PAGE]\n\n';
                $text_vendor += 'If you have any questions, please contact us and we will be pleased to help.\n\n';
                $text_vendor += 'Regards,\n';
                $text_vendor += 'Justin Oliver McCarthy\n';
                $text_vendor += 'Associate Director & Global Legal Executive\n';
                $text_vendor += 'www.share-realestate.com';

                $("#content").val($text_vendor);
            }else{
                $text_provider = "Dear Madam, Sir\n\n";
                $text_provider += "We are seeking a corporate services provider/ law firm in your jurisdiction to incorporate and administer on an ongoing basis, an unlimited number of legal entities.\n\n";
                $text_provider += "Our clients are groups of persons (natural persons and legal entities) who wish to acquire real-estate via a “civil real-estate company” (or similar in your jurisdiction).\n\n";
                $text_provider += "We are presently working with multiple real-estate agents in your jurisdiction, and we have international purchasers seeking your services.\n\n";
                $text_provider += "We hereby invite your firm to become a “partner service provider” for our clients who are purchasing real-estate located in your jurisdiction.\n\n";
                $text_provider += "Our collective real-estate purchase groups are organised, and specific clauses pertaining to the constitutional documents of the foreseen “civil real-estate companies” are already determined.\n\n";
                $text_provider += "We have already prepared due diligence documentation, ready to be delivered, thus alleviating this sometimes difficult  task.\n\n";
                $text_provider += "You are welcome to publicize your services immediately at: www.share-realestate[ADDRESS OF SERVICE PROVIDER REGISTRATION PAGE]\n\n";
                $text_provider += "If you have further questions, we would be pleased to discuss this matter further with you. In the meantime, we invite you to examine our services further at www.share-realestate.com\n\n";
                $text_provider += 'Regards,\n';
                $text_provider += 'Justin Oliver McCarthy\n';
                $text_provider += 'Associate Director & Global Legal Executive\n';
                $text_provider += 'www.share-realestate.com';
                
                $("#content").val($text_provider);
            }
        });
    });
</script>

<section class="hero-section set-bg about-us" data-setbg="/img/bg.jpg">
    <div class="container hero-text text-white">
        <h2>{{ __('Invite people to join the website') }}</h2>
    </div>
</section>

<section class="login-section">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-xl-8">
                <div class="login-form">
                    <form action="/send_invite" method="POST"> 
                            {{ csrf_field() }}

                             @if ($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif

                            <?php if(isset($message)){ ?>
                                <div class="alert alert-success">
                                    {{ $message }}
                                </div>
                            <?php }else if(isset($error_msg)){ ?>
                                <div class="alert alert-danger">
                                    {{ $error_msg }}
                                </div>
                            <?php } ?>        
    
                            <div class="form-group">
                                <label>Subject</label>
                                <input type="text" value="{{ old('subject') }}" name="subject" class="form-control" placeholder="Subject">    
                            </div>
                            <div class="form-group">
                                <label>Target of people</label>
                                <select id="type" class="form-control" name="type">
                                    <option>Select target of people</option>
                                    <option value="vendors" <?php if(old('type') == "vendors") echo 'selected'; ?>>Real-estate agents & Vendors</option>
                                    <option value="providers" <?php if(old('type') == "providers") echo 'selected'; ?>>Corporate service providers/Law firms</option>
                                </select>
                            </div>
                            <div class="form-group">                
                                <label>Content</label>
                                <textarea id="content" class="form-control" placeholder="Email Content" name="content" rows="10">{{ old('content') }}</textarea>
                            </div>
                            <div class="form-group">
                                <label>To</label>
                                <input type="email" value="{{ old('to') }}" name="to" class="form-control" placeholder="Email">    
                                <!-- <small class="">For multiple emails add ; after each email (Example: test@gmail.com; test1@gmail.com)</small> -->
                            </div>
                            
                            <div class="form-group">
                                <button type="submit" class="btn btn-primary">Send</button>
                            </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
