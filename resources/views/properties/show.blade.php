@extends('layouts.main')

@section('main_content')
<section class="hero-section set-bg about-us" data-setbg="/img/bg.jpg">
    <div class="container hero-text text-white">
        <h2>Listing Property</h2>
    </div>
</section>
<section class="login-section">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-xl-8">
                <div class="login-form">
                    
                	<div class="row">
                        <div class="col-lg-6">
                            <label>Title</label>
                            <p>{{ $property->title }}</p> 
                        </div>  
                        <div class="col-lg-6">
                            <label>Description</label>
                            <p>{{ $property->description }}</p>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-12">
                            <label>Images</label>
                            <div>
                                @if(count($property->images) > 0)    
                                @foreach($property->images as $image)
                                    <img class="img_prev_show" src="/images/photo_gallery/properties/{{$image}}" alt="" />
                                @endforeach
                                @else
                                {{'/'}}
                                @endif
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-6">
                            <label>Price</label>
                            <p>{{ $property->price }}EUR</p>    
                        </div>
                        <div class="col-lg-6">
                            <label>Category</label>
                            <p>{{ $property->type_name }}</p>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-6">
                            <label>Country</label>
                            <p>{{ $property->country_name ? $property->country_name : '/' }}</p>
                        </div>
                        <div class="col-lg-6">
                            <label>Address</label>
                            <p>{{ $property->address ? $property->address : '/' }}</p>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-6">
                            <label>Year of Construction</label>
                            <p>{{ $property->year_of_construction ? $property->year_of_construction : '/' }}</p>
                        </div>
                        <div class="col-lg-6">
                            <label>Number of Bedrooms</label>
                            <p>{{ $property->number_of_bedrooms ? $property->number_of_bedrooms : '/' }}</p>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-6">
                            <label>Total Land (m2)</label>
                            <p>{{ $property->total_land ? $property->total_land : '/' }}</p>
                        </div>
                        <div class="col-lg-6">
                            <label>Living Area (m2)</label>
                            <p>{{ $property->living_area ? $property->living_area : '/' }}</p>
                        </div>
                    </div>                        

                    <div class="row">
                        <div class="col-lg-6">
                            <label>Situation</label>
                            <p>
                            	<?php
                            		if($property->situation){
                            			if($property->situation == 'country_side'){
                            				echo "Country Side";
                            			}else{
                            				echo "Town/City";
                            			}
                            		}else{
                            			echo "/";
                            		}
                            	?>
                            </p>
                        </div>     
                        <div class="col-lg-6">
                            <label>Gas</label>
                            <p>{{ $property->gas ? 'Yes' : 'No' }}</p>
                        </div>      
                    </div>
                                        
                    <div class="row">
                        <div class="col-lg-6">
                            <label>Balcony</label>
                            <p>{{ $property->balcony ? 'Yes' : 'No' }}</p>
                        </div>      
                        <div class="col-lg-6">
                            <label>Pool</label>
                            <p>{{ $property->pool ? 'Yes' : 'No' }}</p>
                        </div>      
                    </div>

                    <div class="row">
                        <div class="col-lg-6">
                            <label>Parking</label>
                            <p>{{ $property->parking ? 'Yes' : 'No' }}</p>
                        </div>      
                        <div class="col-lg-6">
                            <label>Central Heating</label>
                            <p>{{ $property->central_heating ? 'Yes' : 'No' }}</p>
                        </div>      
                    </div>

                    <div class="row">
                        <div class="col-lg-6">
                            <label>Promotion Period (Years)</label>
                            <p>{{ $property->promotion_period_years ? $property->promotion_period_years : '/' }}</p>
                        </div>      
                        <div class="col-lg-6">
                            <label>Promotion Period (Months)</label>
                            <p>{{ $property->promotion_period_months ? $property->promotion_period_months : '/' }}</p>
                        </div>      
                    </div>

                    <div class="row">
                        <div class="col-lg-12">
                            <a href="{{ url('property/edit/'. $property->id ) }}" class="site-btn c-btn">Edit</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
