@extends('layouts.main')

@section('main_content')
<section class="hero-section set-bg about-us" data-setbg="/img/bg.jpg">
    <div class="container hero-text text-white">
        <h2>{{ __('My Properties') }}</h2>
    </div>
</section>
<section class="login-section">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-xl-8">
                <div class="login-form group-section">
                    <div class="card-body">
                        @if(count($properties) > 0)
		                	@foreach ($properties as $property)
							    <div class="row group-list">
							        <div class="col-lg-6">
                            			<a href="{{ url('property/'.$property->id) }}" class="title">{{ $property->title }}</a>
							    	</div>
							    	<div class="col-lg-6">
                            	    	<a href="{{ url('property/edit/'. $property->id) }}" class="btn btn-primary edit-property">Edit</a>
										<a href="{{ url('property/'. $property->id) }}" class="btn btn-success property-details">Details</a>
									</div>
						    	</div>
							@endforeach
						@else
							<p>No results found</p>
						@endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection