@extends('layouts.main')

@section('main_content')

<!-- Hero Section Begin -->
<section class="hero-section set-bg about-us" data-setbg="/img/bg.jpg">
    <div class="container hero-text text-white">
        <h2>{{ 'Single Property' }}</h2>
    </div>
</section>

<!-- Single Property Section Begin -->
<div class="single-property">
    <div class="container">
        <div class="row spad-p">
            <div class="col-lg-12">
                <div class="property-title">
                    <h3>{{ $property->title }}</h3>
                    <a href="#"><i class="fa flaticon-placeholder"></i> {{ $property->country_name }}</a>
                </div>
                <div class="property-price">
                    <p>For Sale</p>
                    <span>{{ $property->price }} EUR</span>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="property-img owl-carousel">
                    @if(count($property->images) > 0)    
                    @foreach($property->images as $image)
                        <div class="single-img">
                            <img src="/images/photo_gallery/properties/{{$image}}" alt="">
                        </div>
                    @endforeach
                    @else
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Single Property End
<!-- Single Property Deatails Section Begin -->
<section class="property-details">
    <div class="container">
        <div class="row sp-40 spt-40">
            <div class="col-lg-8">
                <div class="p-ins">
                    <div class="row details-top">
                        <div class="col-lg-12">
                            <div class="t-details">
                                <!-- <div class="register-id"> -->
                                    <!-- <p>Registered ID: <span>0D05426FF1</span></p> -->
                                <!-- </div> -->
                                <div class="popular-room-features single-property">
                                    <!-- <div class="size">
                                        <p>Category</p>
                                        <img src="img/rooms/size.png" alt="">
                                        <i class="flaticon-bath"></i>
                                        <span>@if(isset($property->type_name)){{ $property->type_name }}@else{{ '/' }}@endif</span>
                                    </div> -->
                                    <div class="beds">
                                        <p>Total Land</p>
                                        <img src="img/rooms/size.png" alt="">
                                        <i class="flaticon-bath"></i>
                                        <span>@if(isset($property->total_land)){{ $property->total_land }}@else{{ 0 }}@endif m<sup>2</sup></span>
                                    </div>
                                    <div class="beds">
                                        <p>Living Area</p>
                                        <img src="img/rooms/size.png" alt="">
                                        <i class="flaticon-bath"></i>
                                        <span>{{ $property->living_area }} m<sup>2</sup></span>
                                    </div>
                                    <div class="beds">
                                        <p>Bedrooms</p>
                                        <img src="img/rooms/bed.png" alt="">
                                        <span>{{ $property->number_of_bedrooms }}</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="property-description">
                                <h4>Description</h4>
                                <p>{{ $property->description }}</p>
                            </div>
                            <div class="property-features">
                                <h4>Property Features</h4>
                                <div class="property-table">
                                    <table>
                                        <tr>
                                            <td>Category</td>
                                            <td>@if(isset($property->type_name)){{ $property->type_name }}@else{{'/'}}@endif</td>
                                            <td>&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td>Address</td>
                                            <td>@if(isset($property->address)){{ $property->address }}@else{{'/'}}@endif</td>
                                            <td>&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td>Year of construction</td>
                                            <td>@if(isset($property->year_of_construction)){{ $property->year_of_construction }}@else{{ '/' }}@endif</td>
                                            <td>&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td>Situation</td>
                                            <td>@if(isset($property->situation)){{ $property->situation }}@else{{'/'}}@endif</td>
                                            <td>&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td><img src="@if(isset($property->gas) && $property->gas == 1){{ asset('img/check.png') }}@endif" alt=""> Gas</td>
                                            <td><img src="@if(isset($property->central_heating) && $property->central_heating == 1){{ asset('img/check.png') }}@endif" alt=""> Central heating</td>
                                            <td>&nbsp;</td>    
                                        </tr>
                                        <tr>
                                            <td><img src="@if(isset($property->parking) && $property->parking == 1){{ asset('img/check.png') }}@endif" alt=""> Parking</td>
                                            <td><img src="@if(isset($property->pool) && $property->pool == 1){{ asset('img/check.png') }}@endif" alt=""> Pool</td>
                                            <td><img src="@if(isset($property->balcony) && $property->balcony == 1){{ asset('img/check.png') }}@endif" alt=""> Balcony</td>

                                        </tr>
                                    </table>
                                </div>
                            </div>
                            <!-- <div class="location-map">
                                <h4>Location</h4>
                                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d107002.020096289!2d-96.80666618302782!3d33.06138629992991!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x864c21da13c59513%3A0x62aa036489cd602b!2sPlano%2C+TX%2C+USA!5e0!3m2!1sen!2sbd!4v1558246953339!5m2!1sen!2sbd" allowfullscreen></iframe>
                            </div> -->
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="row pb-30">
                    <div class="col-lg-12">
                        <div class="contact-service">
                            <img src="{{ asset('images/default_provider_profile.png')}}" alt="">
                            <p>Listed by</p>
                            <h5>{{ $property->first_name.' '.$property->last_name }}</h5>
                            <table>
                                <tr>
                                    <td>Number : <span>@if(isset($property->number)){{ $property->number }}@else{{'/'}}@endif</span></td>
                                </tr>
                                <tr>
                                    <td>Comapany : <span>@if(isset($property->company)){{ $property->company }}@else{{ '/' }}@endif</span></td>
                                </tr>
                                <tr>
                                    <td>Comapny Address : <span>@if(isset($property->company_address)){{ $property->company_address }}@else{{'/'}}@endif</span></td>
                                </tr>
                                <tr>
                                    <td>Email : <span>{{ $property->email }}</span></td>
                                </tr>
                            </table>
                            <!-- <a href="#" class="site-btn list-btn">View More Listings</a> -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Single Property Deatails Section End-->
@endsection