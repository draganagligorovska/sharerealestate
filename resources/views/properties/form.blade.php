@extends('layouts.main')

@section('main_content')
<section class="hero-section set-bg about-us" data-setbg="/img/bg.jpg">
    <div class="container hero-text text-white">
        <h2>{{ isset($property->id) ? 'Edit Property' : 'New Listing Property' }}</h2>
    </div>
</section>

<section class="login-section">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-xl-8">
                <div class="login-form">
                    <form method="POST" action="{{ isset($property->id) ? '/property/update/'.$property->id : '/property' }}" enctype="multipart/form-data">
                        {{ csrf_field() }}

                        @if ($errors->any())
                            <div class="row alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        @if($error != 'image')
                                            <li>{{ $error }}</li>
                                        @endif
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        @if(session('property'))
                            <?php $property = session('property'); ?>
                        @endif

                        <div class="row">
                            <div class="col-lg-12">
                                <label>Title</label>
                                <input type="text" name="title" class="form-control" placeholder="Title" value="{{ isset($property->title) ? $property->title : old('title') }}">    
                            </div>
                        </div>

                        <div class="row">                
                            <div class="col-lg-12">
                                <label>Description</label>
                                <textarea name="description" class="form-control" placeholder="Description" rows="5">{{ isset($property->description) ? $property->description : old('description') }}</textarea>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-6">
                                <label>Price</label>
                                <input type="text" name="price" class="form-control" placeholder="Price (EUR)" value="{{ isset($property->price) ? $property->price : old('price') }}">
                            </div>
                            <div class="col-lg-6">
                                <label>Address</label>
                                <input type="text" name="address" class="form-control" placeholder="Address" value="{{ isset($property->address) ? $property->address : old('address') }}">
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-12">
                                Select images to upload:
                                <input type="file" name="files[]" onchange="readURL(this);" id="fileToUpload" value="{{ isset($property->image) ? $property->image : old('image') }}" accept="image/*" multiple/>                    
                            </div>
                        </div>

                        <div id="prev_images" class="row">
                            @if(isset($property->images) && (!empty($property->images)))
                                @foreach($property->images as $img)
                                    @if($img != "")
                                    <div class="col-lg-6">
                                        <div class='img-container'>
                                            <img class="img_prev" src="{{ asset('images/photo_gallery/properties/'.$img) }}" alt=""/>
                                            <button type="button" class="btn btn-danger form-control delete-img">Delete</button>
                                            <input type="hidden" value="{{$img}}" name="images[]"/>
                                        </div>
                                    </div>
                                    @endif
                                @endforeach
                            @endif
                        </div>
               
                        <div class="row">
                            <div class="col-lg-12">
                                <label>Situation</label>
                                <select class="form-control" name="situation">
                                    <option value="">Select Situation</option>
                                    <option value="country_side" <?php if((isset($property->situation) && $property->situation == 'country_side') || old('situation') == 'country_side') echo 'selected'; ?>>Country Side</option>
                                    <option value="town_city" <?php if((isset($property->situation) && $property->situation == 'town_city') || old('situation') == 'town_city') echo 'selected'; ?>>Town/City</option>
                                </select>
                            </div>
                        </div>     

                        <div class="row">
                            <div class="col-lg-6">
                                <label>Category</label>
                                <select class="form-control" name="type">
                                    <option value="">Select type</option>
                                    <?php foreach ($property_types as $key => $value) { ?>
                                        <option value="<?php echo $value->id; ?>" <?php if((isset($property->type) && $property->type == $value->id) || old('type') == $value->id) echo 'selected'; ?>><?php echo $value->name; ?></option>
                                    <?php }?>
                                </select>
                            </div>
                            <div class="col-lg-6">
                                <label>Country</label>
                                <select class="form-control" name="country_id">
                                    <option value="">Select Country</option>
                                    <?php foreach ($countries as $key => $value) { ?>
                                        <option value="<?php echo $value->id; ?>" <?php if((isset($property->country_id) && $property->country_id == $value->id) || old('country_id') == $value->id) echo 'selected'; ?>><?php echo $value->name; ?></option>
                                    <?php }?>
                                </select>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-6">
                                <label>Year of Construction</label>
                                <select class="form-control" name="year_of_construction">
                                    <option value="">Select Year</option>
                                    <?php for($i = date("Y"); $i > 1800; $i--){ ?>
                                        <option value="<?php echo $i; ?>" <?php if((isset($property->year_of_construction) && $property->year_of_construction == $i) || old('year_of_construction') == $i) echo 'selected'; ?>><?php echo $i; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                            <div class="col-lg-6">
                                <label>Number of Bedrooms</label>
                                <select class="form-control" name="number_of_bedrooms">
                                    <option value="">Select Number of Bedrooms</option>
                                    <?php for($i = 1; $i < 100; $i++){ ?>
                                        <option value="<?php echo $i; ?>" <?php if((isset($property->number_of_bedrooms) && $property->number_of_bedrooms == $i) || old('number_of_bedrooms') == $i) echo 'selected'; ?>><?php echo $i; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-6">
                                <label>Total Land (m2)</label>
                                <input type="text" name="total_land" class="form-control" placeholder="Total Land (m2)" value="{{ isset($property->total_land) ? $property->total_land : old('total_land') }}">
                            </div>
                            <div class="col-lg-6">
                                <label>Living Area (m2)</label>
                                <input type="text" name="living_area" class="form-control" placeholder="Living Area (m2)" value="{{ isset($property->living_area) ? $property->living_area : old('living_area') }}">
                            </div>
                        </div>                        

                        <div class="row">
                            <div class="col-lg-6">
                                <input type="checkbox" name="balcony" value="balcony" <?php if((isset($property->balcony) && $property->balcony ==1) || old('balcony') == 'balcony') echo 'checked'; ?>> Balcony<br>
                                <input type="checkbox" name="pool" value="pool" <?php if((isset($property->pool) && $property->pool == 1) || old('pool') == 'pool') echo 'checked'; ?>> Pool<br>
                                <input type="checkbox" name="parking" value="parking" <?php if((isset($property->parking) && $property->parking == 1) || old('parking') == 'parking') echo 'checked'; ?>> Parking<br> 
                           </div>
                           <div class="col-lg-6">
                                <input type="checkbox" name="central_heating" value="central_heating" <?php if((isset($property->central_heating) && $property->central_heating == 1) || old('central_heating') == 'central_heating') echo 'checked'; ?>> Central Heating<br> 
                                <input type="checkbox" name="gas" value="gas" <?php if(isset($property->gas) && $property->gas == 1 || old('gas') == 'gas') echo 'checked'; ?>> Gas<br> 
                           </div>
                        </div>   

                        <div class="row">
                            <div class="col-lg-6">
                                <label>Promotion Period (Years)</label>
                                <select class="form-control" name="promotion_period_years">
                                    <option value="">Select Number of Years</option>
                                    <?php for($i = 1; $i < 11; $i++){ ?>
                                        <option value="<?php echo $i; ?>" <?php if((isset($property->promotion_period_years) && $property->promotion_period_years == $i) || old('promotion_period_years') == $i) echo 'selected'; ?>><?php echo $i; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                            <div class="col-lg-6">
                                <label>Promotion Period (Months)</label>
                                <select class="form-control" name="promotion_period_months">
                                    <option value="">Select Number of Months</option>
                                    <?php for($i = 1; $i < 13; $i++){ ?>
                                        <option value="<?php echo $i; ?>" <?php if((isset($property->promotion_period_months) && $property->promotion_period_months == $i) || old('promotion_period_months') == $i) echo 'selected'; ?>><?php echo $i; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>      

                        <div class="row">
                            <div class="col-lg-12">
                                <button type="submit" class="site-btn c-btn">Save</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
<script type="text/javascript">
    function readURL(input) {
        if (input.files && input.files.length > 0) {
            for(var i = 0; i < input.files.length; i++){
                var reader = new FileReader();
                // var file_name = input.files[i].name;
                
                reader.onload = function (e) {
                    $('#prev_images').append('<div class="img-container"><img class="img_prev" src="#" alt="" /></div>');
                    $('#prev_images img').last().attr('src', e.target.result);
                }
                // $('#prev_images img').last().attr('id', file_name);

                reader.readAsDataURL(input.files[i]);
            }
        }
    }

    $(document).on( "click", ".delete-img", function(e) {

        $(this).closest('div').remove();

    });
</script>
@endsection
