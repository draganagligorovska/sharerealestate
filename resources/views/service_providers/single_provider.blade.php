@extends('layouts.main')

@section('main_content')

<!-- Hero Section Begin -->
<section class="hero-section set-bg about-us" data-setbg="/img/bg.jpg">
    <div class="container hero-text text-white">
        <h2>{{ 'Single Service Provider' }}</h2>
    </div>
</section>

<!-- Single Property Section Begin -->
<div class="single-property">
    <div class="container">
        <div class="row spad-p">
            <div class="col-lg-12">
                <div class="property-title">
                    <h3>{{ $provider->first_name.' '.$provider->last_name }}</h3>
                    <a href="#"><i class="fa flaticon-placeholder"></i> {{ $provider->country_name }}</a>
                </div>
                <div class="property-price">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                
            </div>
        </div>
    </div>
</div>
<!-- Single Property End
<!-- Single Property Deatails Section Begin -->
<section class="property-details">
    <div class="container">
        <div class="row sp-40 spt-40">
            <div class="col-lg-4">
                <div class="row pb-30">
                    <div class="col-lg-12">
                        <div class="contact-service">
                            <img src="{{ asset('images/default_provider_profile.png')}}" alt="">
                            <p>Listed by</p>
                            <h5>{{ $provider->first_name.' '.$property->last_name }}</h5>
                            <table>
                                <tr>
                                    <td>Number : <span>@if(isset($provider->number)){{ $property->number }}@else{{'/'}}@endif</span></td>
                                </tr>
                                <tr>
                                    <td>Comapany : <span>@if(isset($provider->company)){{ $property->company }}@else{{ '/' }}@endif</span></td>
                                </tr>
                                <tr>
                                    <td>Comapny Address : <span>@if(isset($provider->company_address)){{ $provider->company_address }}@else{{'/'}}@endif</span></td>
                                </tr>
                                <tr>
                                    <td>Email : <span>{{ $provider->email }}</span></td>
                                </tr>
                            </table>
                            <!-- <a href="#" class="site-btn list-btn">View More Listings</a> -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Single Property Deatails Section End-->
@endsection