@extends('layouts.main')

@section('main_content')
<section class="hero-section set-bg about-us" data-setbg="/img/bg.jpg">
    <div class="container hero-text text-white">
        <h2>My Groups</h2>
    </div>
</section>
<section class="login-section">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-xl-8">
                <div class="login-form group-section">
                    <div class="card-body">
                        @if(count($groups) > 0)
                        @foreach ($groups as $group)
                            <div class="row group-list">
                                <div class="col-lg-6">
                                    <a href="{{ url('group/'.$group->id) }}" class="title">{{ $group->name }}</a>
                                </div>
                                <div class="col-lg-6">
                                    <a href="{{ url('group/edit/'. $group->id) }}" class="btn btn-success">Edit</a>
                                    <a href="{{ url('group/'. $group->id) }}" class="btn btn-primary">Details</a>
                                </div>
                            </div>
                        @endforeach
                        @else
                            <p>No results found</p>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection