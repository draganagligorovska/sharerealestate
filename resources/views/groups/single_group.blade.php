@extends('layouts.main')

@section('main_content')

<!-- Hero Section Begin -->
<section class="hero-section set-bg about-us" data-setbg="/img/bg.jpg">
    <div class="container hero-text text-white">
        <h2>{{ 'Single Group' }}</h2>
    </div>
</section>

<!-- Single Property Section Begin -->
<div class="single-property">
    <div class="container">
        <div class="row spad-p">
            <div class="col-lg-12">
                <div class="property-title">
                    <h3>{{ $group->name }}</h3>
                    <a href="#"><i class="fa flaticon-placeholder"></i> {{ $group->country_name }}</a>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Single Property End
<!-- Single Property Deatails Section Begin -->
<section class="property-details">
    <div class="container">
        <div class="row sp-40 spt-40">
            <div class="col-lg-8">
                <div class="p-ins">
                    <div class="row details-top">
                        <div class="col-lg-12">
                            <div class="t-details">
                                <!-- <div class="register-id"> -->
                                    <!-- <p>Registered ID: <span>0D05426FF1</span></p> -->
                                <!-- </div> -->
                                <div class="popular-room-features single-property">
                                    <div class="size">
                                        <p>Category</p>
                                        <!-- <img src="img/rooms/size.png" alt=""> -->
                                        <!-- <i class="flaticon-bath"></i> -->
                                        <span>@if(isset($group->type_name)){{ $group->type_name }}@else{{ '/' }}@endif</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="property-description">
                                <h4>Description</h4>
                                <p>{{ $group->description }}</p>
                            </div>
                            <div class="property-features">
                                <h4>Details of target property</h4>
                                <div class="property-table">
                                    <table>
                                        <tr>
                                            <td>Region</td>
                                            <td>@if(isset($group->region)){{ $group->region }}@else{{'/'}}@endif</td>
                                            <td>&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td>Capital per member</td>
                                            <td>@if(isset($group->capital_per_member)){{ $group->capital_per_member }}@else{{'/'}}@endif</td>
                                            <td>&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td>Number of bedrroms</td>
                                            <td>@if(isset($group->num_of_bedrooms)){{ $group->num_of_bedrooms }}@else{{ '/' }}@endif</td>
                                            <td>&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td>Condition</td>
                                            <td>@if(isset($group->condition)){{ ucfirst($group->condition) }}@else{{'/'}}@endif</td>
                                            <td>&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td><img src="@if(isset($group->gas) && $group->gas == 1){{ asset('img/check.png') }}@endif" alt=""> Gas</td>
                                            <td><img src="@if(isset($group->central_heating) && $group->central_heating == 1){{ asset('img/check.png') }}@endif" alt=""> Central heating</td>
                                            <td>&nbsp;</td>    
                                        </tr>
                                        <tr>
                                            <td><img src="@if(isset($group->parking) && $group->parking == 1){{ asset('img/check.png') }}@endif" alt=""> Parking</td>
                                            <td><img src="@if(isset($group->pool) && $group->pool == 1){{ asset('img/check.png') }}@endif" alt=""> Pool</td>
                                            <td><img src="@if(isset($group->balcony) && $group->balcony == 1){{ asset('img/check.png') }}@endif" alt=""> Balcony</td>
                                        </tr>
                                        <tr>
                                            <td>Urban/Sub-Urban/Countryside</td>
                                            <td>@if(isset($group->location_type)){{ ucfirst($group->location_type) }}@else{{'/'}}@endif</td>
                                            <td>&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td>Use of property</td>
                                            <td>@if(isset($group->use_of_property)){{ ucfirst($group->use_of_property) }}@else{{'/'}}@endif</td>
                                            <td>&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td>Year of construction range</td>
                                            <td>( @if(isset($group->year_of_construction_from)){{ $group->year_of_construction_from }}@else{{'/'}}@endif - @if(isset($group->year_of_construction_to)){{ $group->year_of_construction_to }}@else{{'/'}}@endif )</td>
                                            <td>&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td>Total land range</td>
                                            <td>( @if(isset($group->total_land_from)){{ $group->total_land_from.' m2' }}@else{{'/'}}@endif - @if(isset($group->total_land_to)){{ $group->total_land_to.' m2' }}@else{{'/'}}@endif )</td>
                                            <td>&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td>Living area range</td>
                                            <td>( @if(isset($group->living_area_from)){{ $group->living_area_from.' m2' }}@else{{'/'}}@endif - @if(isset($group->living_area_to)){{ $group->living_area_to .' m2' }}@else{{'/'}}@endif )</td>
                                            <td>&nbsp;</td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                            <div class="property-features">
                                <h4>Group profile</h4>
                                <div class="property-table">
                                    <table>
                                        <tr>
                                            <td>Max members</td>
                                            <td>@if(isset($group->max_num_of_members)){{ $group->max_num_of_members }}@else{{'/'}}@endif</td>
                                            <td>&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td>Total capital requirements</td>
                                            <td>@if(isset($group->total_capital_requirements)){{ $group->total_capital_requirements }}@else{{'/'}}@endif</td>
                                            <td>&nbsp;</td>
                                        </tr>
                                    </table>
                                </div>
                            </div>

                            <div class="property-features">
                                <h4>Group member profiles</h4>
                                <div class="property-table">
                                    <table>
                                        <tr>
                                            <td><img src="@if(isset($group->gay_friendly) && $group->gay_friendly == 1){{ asset('img/check.png') }}@endif" alt=""> Gay Friendly</td>
                                            <td><img src="@if(isset($group->smoker_tolerant) && $group->smoker_tolerant == 1){{ asset('img/check.png') }}@endif" alt=""> Smoker Tolerant</td>
                                            <td><img src="@if(isset($group->alcohol_tolerant) && $group->alcohol_tolerant == 1){{ asset('img/check.png') }}@endif" alt=""> Alcohol Tolerant</td>    
                                        </tr>
                                        <tr>
                                            <td><img src="@if(isset($group->pet_friendly) && $group->pet_friendly == 1){{ asset('img/check.png') }}@endif" alt=""> Pet Friendly</td>
                                            <td><img src="@if(isset($group->family_friendly) && $group->family_friendly == 1){{ asset('img/check.png') }}@endif" alt=""> Family Friendly</td>
                                            <td><img src="@if(isset($group->disability_accommodating) && $group->disability_accommodating == 1){{ asset('img/check.png') }}@endif" alt=""> Disability Accommodating</td>
                                        </tr>
                                        <tr>
                                            <td>Age Range</td>
                                            <td>( @if(isset($group->age_from)){{ $group->age_from }}@else{{'/'}}@endif - @if(isset($group->age_to)){{ $group->age_to }}@else{{'/'}}@endif )</td>
                                            <td>&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td>Living With</td>
                                            <td>@if(isset($group->living_with)){{ ucfirst($group->living_with) }}@else{{'/'}}@endif</td>
                                            <td>&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td>Sexual Orientation</td>
                                            <td>@if(isset($group->sexual_orientation)){{ ucfirst($group->sexual_orientation) }}@else{{'/'}}@endif</td>
                                            <td>&nbsp;</td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                            <div class="property-features">
                                <h4>Nationalities</h4>
                                <div class="property-table">
                                    <table>
                                        @if(count($nationalities) > 0)
                                            @foreach($nationalities as $n)
                                            <tr>
                                                <td>{{$n['name']}}</td>
                                            </tr>
                                            @endforeach
                                        @else
                                            <tr>
                                                <td> / </td>
                                            </tr>
                                        @endif
                                    </table>
                                </div>
                            </div>
                            <div class="property-features">
                                <h4>Religions</h4>
                                <div class="property-table">
                                    <table>
                                        @if(count($religions) > 0)
                                            @foreach($religions as $n)
                                            <tr>
                                                <td>{{$n['name']}}</td>
                                            </tr>
                                            @endforeach
                                        @else
                                            <tr>
                                                <td> / </td>
                                            </tr>
                                        @endif
                                    </table>
                                </div>
                            </div>
                            <div class="property-features">
                                <h4>Languages</h4>
                                <div class="property-table">
                                    <table>
                                        @if(count($languages) > 0)
                                            @foreach($languages as $n)
                                            <tr>
                                                <td>{{$n['name']}}</td>
                                            </tr>
                                            @endforeach
                                        @else
                                            <tr>
                                                <td> / </td>
                                            </tr>
                                        @endif
                                    </table>
                                </div>
                            </div>
                            <div class="property-features">
                                <h4>Professions</h4>
                                <div class="property-table">
                                    <table>
                                        @if(count($professions) > 0)
                                            @foreach($professions as $n)
                                            <tr>
                                                <td>{{$n['name']}}</td>
                                            </tr>
                                            @endforeach
                                        @else
                                            <tr>
                                                <td> / </td>
                                            </tr>
                                        @endif
                                    </table>
                                </div>
                            </div>
                            <div class="property-features">
                                <h4>SPV</h4>
                                <div class="property-table">
                                    <table>
                                        <tr>
                                            <td>Contract</td>
                                            <td>@if(isset($group->contract)){{ ucfirst($group->contract) }}@else{{'/'}}@endif</td>
                                            <td>&nbsp;</td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="row pb-30">
                    <div class="col-lg-12">
                        <div class="contact-service">
                            <img src="{{ asset('images/default_provider_profile.png')}}" alt="">
                            <p>Listed by</p>
                            <h5>{{ $group->first_name.' '.$group->last_name }}</h5>
                            <table>
                                <tr>
                                    <td>Number : <span>@if(isset($group->number)){{ $group->number }}@else{{'/'}}@endif</span></td>
                                </tr>
                                <tr>
                                    <td>Email : <span>{{ $group->email }}</span></td>
                                </tr>
                            </table>
                            <!-- <a href="#" class="site-btn list-btn">View More Listings</a> -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Single Property Deatails Section End-->
@endsection