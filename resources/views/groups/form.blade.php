@extends('layouts.main')

@section('main_content')


<script type="text/javascript">
    $( document ).ready(function() {
        $('.multiple-select').select2();
    });
</script>
<!-- Hero Section Begin -->
<section class="hero-section set-bg about-us" data-setbg="/img/bg.jpg">
    <div class="container hero-text text-white">
        <h2>{{ isset($group->id) ? 'Edit Group' : 'Create Group' }}</h2>
    </div>
</section>
<!-- Hero Section End -->
<section class="login-section">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-xl-8">
                <div class="login-form group-section">
                    <form method="POST" action="{{ isset($group->id) ? '/group/update/'.$group->id : '/group' }}" enctype="multipart/form-data">
                        {{ csrf_field() }}

                        @if ($errors->any())
                            <div class="row alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        @if($error != 'image')
                                            <li>{{ $error }}</li>
                                        @endif
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        @if(session('group'))
                            <?php $group = session('group'); ?>
                        @endif

                            <div class="row">
                                <div class="col-lg-12">
                                    <label>Name</label>
                                    <input type="text" name="name" class="form-control" placeholder="Name" value="{{ isset($group->name) ? $group->name : old('name') }}"/>    
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-lg-12">
                                   <h4>Details of target property</h4>
                                </div>
                            </div>

                            <div class="row">  
                                <div class="col-lg-12">              
                                    <label>Description</label>
                                    <textarea name="description" class="form-control" placeholder="Description" rows="5">{{ isset($group->description) ? $group->description : old('description') }}</textarea>
                                </div>
                            </div>
                   
                            <div class="row">
                                <div class="col-lg-6">
                                    <label>Region</label>
                                    <input type="text" name="region" class="form-control" placeholder="Region" value="{{ isset($group->region) ? $group->region : old('region') }}">
                                </div>
                                <div class="col-lg-6">
                                    <label>Country</label>
                                    <select class="form-control" name="country_id">
                                        <option value="">Select Country</option>
                                        <?php foreach ($countries as $key => $value) { ?>
                                            <option value="<?php echo $value->id; ?>" <?php if((isset($group->country_id) && ($group->country_id == $value->id)) || old('country_id') == $value->id) echo 'selected'; ?>><?php echo $value->name; ?></option>
                                        <?php }?>
                                    </select>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-lg-6">
                                    <label>Category</label>
                                    <select class="form-control" name="category" id="category">
                                        <option value="">Category</option>
                                        <?php foreach ($property_types as $key => $value) { ?>
                                            <option value="<?php echo $value->id; ?>" <?php if((isset($group->type_id) && $group->type_id == $value->id) || old('category') == $value->id) echo 'selected'; ?>><?php echo $value->name; ?></option>
                                        <?php }?>
                                    </select>
                                </div>
                                <div class="col-lg-6">
                                    <label>Capital Contribution Per Member</label>
                                    <select class="form-control" name="capital_per_member" id="capital_per_member">
                                        <option value="">Capital Contribution Per Member</option>
                                        <option value="1_to_5000" <?php if((isset($group->capital_per_member) && $group->capital_per_member == '1_to_5000') || old('capital_per_member') == '1_to_5000') echo 'selected'; ?>>1 to 5,000 EUR</option>
                                        <option value="5001_to_10000" <?php if((isset($group->capital_per_member) && $group->capital_per_member == '5001_to_10000') || old('capital_per_member') == '5001_to_10000') echo 'selected'; ?>>5,001 to 10,000 EUR</option>
                                        <option value="10001_to_50000" <?php if((isset($group->capital_per_member) && $group->capital_per_member == '10001_to_50000') || old('capital_per_member') == '10001_to_50000') echo 'selected'; ?>>10,001 to 50,000 EUR</option>
                                        <option value="50001_to_100000" <?php if((isset($group->capital_per_member) && $group->capital_per_member == '50001_to_100000') || old('capital_per_member') == '50001_to_100000') echo 'selected'; ?>>50,001 to 100,000 EUR</option>
                                        <option value="100000_and_more" <?php if((isset($group->capital_per_member) && $group->capital_per_member == '100000_and_more') || old('capital_per_member') == '100000_and_more') echo 'selected'; ?>>100,000 EUR and more</option>    
                                    </select>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-lg-6">
                                    <label>Number of Bedrooms</label>
                                    <select class="form-control" name="num_of_bedrooms">
                                        <option value="">Select Number of Bedrooms</option>
                                        <?php for($i = 1; $i < 100; $i++){ ?>
                                            <option value="<?php echo $i; ?>" <?php if((isset($group->num_of_bedrooms) && $group->num_of_bedrooms == $i) || old('num_of_bedrooms') == $i) echo 'selected'; ?>><?php echo $i; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                                <div class="col-lg-6">
                                    <label>Condition</label>
                                    <select class="form-control" name="condition">
                                        <option value="">Condition</option>
                                        <option value="new" <?php if((isset($group->condition) && $group->condition == 'new') || old('condition') == 'new') echo 'selected'; ?>>New</option>
                                        <option value="good_condition" <?php if((isset($group->condition) && $group->condition == 'good_condition') || old('condition') == 'good_condition') echo 'selected'; ?>>Good Condition</option>
                                        <option value="need_renovation" <?php if((isset($group->condition) && $group->condition == 'need_renovation') || old('condition') == 'need_renovation') echo 'selected'; ?>>Need Renovation</option>
                                    </select>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-lg-6">
                                        <input type="checkbox" name="balcony" value="balcony" <?php if((isset($group->balcony) && $group->balcony ==1) || old('balcony') == 'balcony') echo 'checked'; ?>> Balcony<br>
                                        <input type="checkbox" name="pool" value="pool" <?php if((isset($group->pool) && $group->pool == 1) || old('pool') == 'pool') echo 'checked'; ?>> Pool<br>
                                        <input type="checkbox" name="parking" value="parking" <?php if((isset($group->parking) && $group->parking == 1) || old('parking') == 'parking') echo 'checked'; ?>> Parking<br> 
                                </div>
                                <div class="col-lg-6">
                                        <input type="checkbox" name="central_heating" value="central_heating" <?php if((isset($group->central_heating) && $group->central_heating == 1) || old('central_heating') == 'central_heating') echo 'checked'; ?>> Central Heating<br> 
                                        <input type="checkbox" name="gas" value="gas" <?php if(isset($group->gas) && $group->gas == 1 || old('gas') == 'gas') echo 'checked'; ?>> Gas<br> 
                                </div> 
                            </div>

                            <div class="row">
                                <div class="col-lg-6">
                                    <label>Urban/Sub-Urban/Countryside</label>
                                    <select class="form-control" name="situation">
                                        <option value="">Urban/Sub-Urban/Countryside</option>
                                        <option value="urban" <?php if((isset($group->location_type) && $group->location_type == 'urban') || old('situation') == 'urban') echo 'selected'; ?>>Urban</option>
                                        <option value="sub_urban" <?php if((isset($group->location_type) && $group->location_type == 'sub_urban') || old('situation') == 'sub_urban') echo 'selected'; ?>>Sub-Urban</option>
                                        <option value="countryside" <?php if((isset($group->location_type) && $group->location_type == 'countryside') || old('situation') == 'countryside') echo 'selected'; ?>>Countryside</option>
                                    </select>
                                </div>
                                <div class="col-lg-6">
                                    <label>Use of property</label>
                                    <select class="form-control" name="use_of_property">
                                        <option value="">Use of property</option>
                                        <option value="co_living_purchase" <?php if((isset($group->use_of_property) && $group->use_of_property == 'co_living_purchase') || old('use_of_property') == 'co_living_purchase') echo 'selected'; ?>>Co-living (purchase)</option>
                                        <option value="co_living_rental" <?php if((isset($group->use_of_property) && $group->use_of_property == 'co_living_rental') || old('use_of_property') == 'co_living_rental') echo 'selected'; ?>>Co-living (rental)</option>
                                        <option value="co_working_purchase" <?php if((isset($group->use_of_property) && $group->use_of_property == 'co_working_purchase') || old('use_of_property') == 'co_working_purchase') echo 'selected'; ?>>Co-working (purchase)</option>
                                        <option value="co_working_rental" <?php if((isset($group->use_of_property) && $group->use_of_property == 'co_working_rental') || old('use_of_property') == 'co_working_rental') echo 'selected'; ?>>Co-working (rental)</option>
                                        <option value="shared_vacation_property" <?php if((isset($group->use_of_property) && $group->use_of_property == 'shared_vacation_property') || old('use_of_property') == 'shared_vacation_property') echo 'selected'; ?>>Shared vacation property (purchase)</option>
                                        <option value="investment_property" <?php if((isset($group->use_of_property) && $group->use_of_property == 'investment_property') || old('use_of_property') == 'investment_property') echo 'selected'; ?>>Investment property</option>
                                        <option value="business_venture_purchase" <?php if((isset($group->use_of_property) && $group->use_of_property == 'business_venture_purchase') || old('use_of_property') == 'business_venture_purchase') echo 'selected'; ?>>Business venture (purchase)</option>
                                        <option value="business_venture_rental" <?php if((isset($group->use_of_property) && $group->use_of_property == 'business_venture_rental') || old('use_of_property') == 'business_venture_rental') echo 'selected'; ?>>Business venture (rental)</option>
                                        <option value="other">Other</option>
                                    </select>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-lg-12">
                                    <label>Year of Construction Range</label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-6">
                                    <label>From</label>
                                    <select class="form-control" name="year_of_construction_from">
                                        <option value="">Select Year</option>
                                        <?php for($i = date("Y"); $i > 1800; $i--){ ?>
                                            <option value="<?php echo $i; ?>" <?php if((isset($group->year_of_construction_from) && $group->year_of_construction_from == $i) || old('year_of_construction_from') == $i) echo 'selected'; ?>><?php echo $i; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                                <div class="col-lg-6">
                                    <label>To</label>
                                    <select class="form-control" name="year_of_construction_to">
                                        <option value="">Select Year</option>
                                        <?php for($i = date("Y"); $i > 1800; $i--){ ?>
                                            <option value="<?php echo $i; ?>" <?php if((isset($group->year_of_construction_to) && $group->year_of_construction_to == $i) || old('year_of_construction_to') == $i) echo 'selected'; ?>><?php echo $i; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-lg-12">
                                    <label>Total Land Range</label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="left-side col-lg-6">
                                    <label>From</label>
                                    <input type="text" name="total_land_from" class="form-control" placeholder="Total Land (m2)" value="{{ isset($group->total_land_from) ? $group->total_land_from : old('total_land_from') }}">
                                </div>

                                <div class="right-side col-lg-6">
                                    <label>To</label>
                                    <input type="text" name="total_land_to" class="form-control" placeholder="Total Land (m2)" value="{{ isset($group->total_land_to) ? $group->total_land_to : old('total_land_to') }}">
                                </div>
                            </div>
                    
                            <div class="row">
                                <div class="col-lg-12">
                                    <label>Living Area Range</label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="left-side col-lg-6">
                                    <label>From</label>
                                    <input type="text" name="living_area_from" class="form-control" placeholder="Living Area (m2)" value="{{ isset($group->living_area_from) ? $group->living_area_from : old('living_area_from') }}">
                                </div>  
                                <div class="right-side col-lg-6">
                                    <label>To</label>
                                    <input type="text" name="living_area_to" class="form-control" placeholder="Living Area (m2)" value="{{ isset($group->living_area_to) ? $group->living_area_to : old('living_area_to') }}">
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-lg-12">
                                    <h4>Group profile</h4>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-lg-6">
                                    <label>Max Number of Members</label>
                                    <select class="form-control" name="max_num_of_members">
                                        <option value="">Select Number of Members</option>
                                        <?php for($i = 1; $i < 100; $i++){ ?>
                                            <option value="<?php echo $i; ?>" <?php if((isset($group->max_num_of_members) && $group->max_num_of_members == $i) || old('max_num_of_members') == $i) echo 'selected'; ?>><?php echo $i; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                                <div class="col-lg-6">
                                    <label>Total Capital Requirements</label>
                                    <select class="form-control" name="total_capital_requirements" id="total_capital_requirements">
                                        <option value="">Total Capital Requirements</option>
                                        <option value="1_to_5000" <?php if((isset($group->total_capital_requirements) && $group->total_capital_requirements == '1_to_5000') || old('total_capital_requirements') == '1_to_5000') echo 'selected'; ?>>1 to 5,000 EUR</option>
                                        <option value="5001_to_10000" <?php if((isset($group->total_capital_requirements) && $group->total_capital_requirements == '5001_to_10000') || old('total_capital_requirements') == '5001_to_10000') echo 'selected'; ?>>5,001 to 10,000 EUR</option>
                                        <option value="10001_to_50000" <?php if((isset($group->total_capital_requirements) && $group->total_capital_requirements == '10001_to_50000') || old('total_capital_requirements') == '10001_to_50000') echo 'selected'; ?>>10,001 to 50,000 EUR</option>
                                        <option value="50001_to_100000" <?php if((isset($group->total_capital_requirements) && $group->total_capital_requirements == '50001_to_100000') || old('total_capital_requirements') == '50001_to_100000') echo 'selected'; ?>>50,001 to 100,000 EUR</option>
                                        <option value="100000_and_more" <?php if((isset($group->total_capital_requirements) && $group->total_capital_requirements == '100000_and_more') || old('total_capital_requirements') == '100000_and_more') echo 'selected'; ?>>100,000 EUR and more</option>
                                    </select>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-lg-12">
                                    <h4>Group member profiles</h4>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="row">
                                        <div class="col-sm">
                                            <input type="checkbox" name="gay_friendly" value="gay_friendly" <?php if((isset($group->gay_friendly) && $group->gay_friendly ==1) || old('gay_friendly') == 'gay_friendly') echo 'checked'; ?>> Gay friendly<br>
                                            <input type="checkbox" name="smoker_tolerant" value="smoker_tolerant" <?php if((isset($group->smoker_tolerant) && $group->smoker_tolerant == 1) || old('smoker_tolerant') == 'smoker_tolerant') echo 'checked'; ?>> Smoker Tolerant<br>
                                            <input type="checkbox" name="alcohol_tolerant" value="alcohol_tolerant" <?php if((isset($group->alcohol_tolerant) && $group->alcohol_tolerant == 1) || old('alcohol_tolerant') == 'alcohol_tolerant') echo 'checked'; ?>> Alcohol Tolerant<br> 
                                        </div>
                                        <div class="col-sm">
                                            <input type="checkbox" name="pet_friendly" value="pet_friendly" <?php if((isset($group->pet_friendly) && $group->pet_friendly == 1) || old('pet_friendly') == 'pet_friendly') echo 'checked'; ?>> Pet Friendly<br> 
                                            <input type="checkbox" name="family_friendly" value="family_friendly" <?php if(isset($group->family_friendly) && $group->family_friendly == 1 || old('family_friendly') == 'family_friendly') echo 'checked'; ?>> Family Friendly<br> 
                                            <input type="checkbox" name="disability_accommodating" value="disability_accommodating" <?php if(isset($group->disability_accommodating) && $group->disability_accommodating == 1 || old('disability_accommodating') == 'disability_accommodating') echo 'checked'; ?>> Disability Accommodating<br> 
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-lg-12">
                                    <label>Age Range</label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="left-side col-lg-6">
                                    <label>From</label>
                                    <select class="form-control" name="age_from">
                                        <option value="">Select Ages</option>
                                        <?php for($i = 1; $i < 130; $i++){ ?>
                                            <option value="<?php echo $i; ?>" <?php if((isset($group->age_from) && $group->age_from == $i) || old('age_from') == $i) echo 'selected'; ?>><?php echo $i; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                                <div class="right-side col-lg-6">
                                    <label>To</label>
                                    <select class="form-control" name="age_to">
                                        <option value="">Select Ages</option>
                                        <?php for($i = 1; $i < 130; $i++){ ?>
                                            <option value="<?php echo $i; ?>" <?php if((isset($group->age_to) && $group->age_to == $i) || old('age_to') == $i) echo 'selected'; ?>><?php echo $i; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-lg-6">
                                    <label>Living with</label>
                                    <select class="form-control" name="living_with">
                                        <option value="">Living with</option>
                                        <option value="family" <?php if((isset($group->living_with) && $group->living_with == 'family') || old('living_with') == 'family') echo 'selected'; ?>>Family</option>
                                        <option value="single" <?php if((isset($group->living_with) && $group->living_with == 'single') || old('living_with') == 'single') echo 'selected'; ?>>Single</option>
                                        <option value="kids" <?php if((isset($group->living_with) && $group->living_with == 'kids') || old('living_with') == 'kids') echo 'selected'; ?>>Kids</option>
                                    </select>
                                </div>
                                <div class="col-lg-6">
                                    <label>Sexual orientation</label>
                                    <select class="form-control" name="sexual_orientation">
                                        <option value="">Sexual orientation</option>
                                        <option value="gay" <?php if((isset($group->sexual_orientation) && $group->sexual_orientation == 'gay') || old('sexual_orientation') == 'gay') echo 'selected'; ?>>Gay</option>
                                        <option value="straight" <?php if((isset($group->sexual_orientation) && $group->sexual_orientation == 'straight') || old('sexual_orientation') == 'straight') echo 'selected'; ?>>Straight</option>
                                    </select>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-lg-6">
                                    <label>Religions</label>
                                    <select multiple="multiple" class="multiple-select form-control" name="religion_ids[]">
                                        @foreach($religions as $religion)
                                        <option value="{{$religion->id}}" <?php if((isset($religion_ids) && in_array($religion->id, $religion_ids)) || ( old('religion_ids') != null && in_array($religion->id, old('religion_ids'))) ) echo 'selected'; ?>>{{$religion->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-lg-6">
                                    <label>Languages</label>
                                    <select multiple="multiple" class="multiple-select form-control" name="language_ids[]">
                                        @foreach($languages as $language)
                                            <option value="{{$language->id}}" <?php if((isset($language_ids) && in_array($language->id, $language_ids)) || ( old('language_ids') != null && in_array($language->id, old('language_ids')))) echo 'selected'; ?>>{{$language->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-lg-6">
                                    <label>Professions</label>
                                    <select multiple="multiple" class="multiple-select form-control" name="profession_ids[]">
                                        @foreach($professions as $profession)
                                            <option value="{{$profession->id}}" <?php if((isset($profession_ids) && in_array($profession->id, $profession_ids)) || ( old('profession_ids') != null && in_array($profession->id, old('profession_ids'))))  echo 'selected'; ?>>{{$profession->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-lg-6">
                                    <label>Nationalities</label>
                                    <select multiple="multiple" name="nationality_ids[]" class="multiple-select form-control">
                                        @foreach($nationalities as $id => $value)
                                            <option value="<?php echo $value->id; ?>" <?php if((isset($nationality_ids) && in_array($id, $nationality_ids)) || ( old('nationality_ids') != null && in_array($id, old('nationality_ids')))) echo 'selected'; ?>><?php echo $value->name; ?></option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-lg-12">
                                    <p>&nbsp;</p>
                                    <p>Consider a Special Purpose Vehicle for your group! Lear more <a href="{{ url('rules') }}" target="_block">here</a>.</p>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-lg-12">
                                    <label>Contract</label>
                                    <select class="form-control" name="contract">
                                        <option value="">Select Contract</option>
                                        <option value="residentional" <?php if((isset($group->contract) && $group->contract == 'residentional') || old('contract') == 'residentional') echo 'selected'; ?>>Residentional</option>
                                        <option value="vacation" <?php if((isset($group->contract) && $group->contract == 'vacation') || old('contract') == 'vacation') echo 'selected'; ?>>Vacation</option>
                                        <option value="investment" <?php if((isset($group->contract) && $group->contract == 'investment') || old('contract') == 'investment') echo 'selected'; ?>>Investment</option>
                                    </select>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-lg-12">
                                    <button type="submit" class="site-btn c-btn">Save</button>
                                </div>
                            </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection