@extends('layouts.main')

@section('main_content')

<!-- Hero Section Begin -->
<section class="hero-section set-bg about-us" data-setbg="/img/bg.jpg">
    <div class="container hero-text text-white">
        <h2>Listing Group</h2>
    </div>
</section>
<!-- Hero Section End -->
<section class="login-section">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-xl-8">
                <div class="login-form group-section">

                    <div class="row">
                        <div class="col-lg-12">
                            <label>Name</label>
                            <p>{{ $group->name }}</p>   
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-12">
                           <h4>Details of target property</h4>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-12">
                            <label>Description</label>
                            <p>{{ $group->description }}</p>
                        </div>
                    </div>
                    
                    <div class="row">
                        <div class="col-lg-6">
                            <label>Region</label>
                            <p>{{ $group->region ? $group->region : '/' }}</p>    
                        </div>
                        <div class="col-lg-6">
                            <label>Country</label>
                            <p>{{ $group->country_name ? $group->country_name : '/' }}</p>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-6">
                            <label>Category</label>
                            <p>{{ $group->type_name ? $group->type_name : '/'}}</p>
                        </div>
                        <div class="col-lg-6"> 
                            <label>Capital Per Member</label>
                            <p>{{ $group->capital_per_member ? $group->capital_per_member : '/' }} EUR</p>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-6">
                            <label>Number of Bedrooms</label>
                            <p>{{ $group->num_of_bedrooms ? $group->num_of_bedrooms : '/' }}</p>
                        </div>
                        <div class="col-lg-6">
                            <label>Gas</label>
                            <p>{{ $group->gas ? 'Yes' : 'No' }}</p>
                        </div>
                    </div>      

                    <div class="row">
                        <div class="col-lg-6">
                            <label>Balcony</label>
                            <p>{{ $group->balcony ? 'Yes' : 'No' }}</p>
                        </div>
                        <div class="col-lg-6">
                            <label>Pool</label>
                            <p>{{ $group->pool ? 'Yes' : 'No' }}</p>
                        </div>
                    </div>      

                    <div class="row">
                        <div class="col-lg-6">
                            <label>Parking</label>
                            <p>{{ $group->parking ? 'Yes' : 'No' }}</p>
                        </div>
                        <div class="col-lg-6">
                            <label>Central Heating</label>
                            <p>{{ $group->central_heating ? 'Yes' : 'No' }}</p>
                        </div>
                    </div>      

                    <div class="row">
                        <div class="col-lg-6">
                            <label>Situation</label>
                            <p>
                                <?php
                                    if($group->location_type){
                                        if($group->location_type == 'country_side'){
                                            echo "Country Side";
                                        }else{
                                            echo "Town/City";
                                        }
                                    }else{
                                        echo "/";
                                    }
                                ?>
                            </p>
                        </div>  
                        <div class="col-lg-6">
                            <label>Use of Property</label>
                            <p>{{ $group->use_of_property ? $group->use_of_property : '/' }}</p>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-12">
                            <label>Condition</label>
                            <p>{{ $group->condition ? $group->condition : '/' }}</p>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-6">
                            <label>Year of Construction From</label>
                            <p>{{ $group->year_of_construction_from ? $group->year_of_construction_from : '/' }}</p>
                        </div>
                        <div class="col-lg-6">
                            <label>Year of Construction To</label>
                            <p>{{ $group->year_of_construction_to ? $group->year_of_construction_to : '/' }}</p>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-6">
                            <label>Total Land From</label>
                            <p>{{ $group->total_land_from ? $group->total_land_from : '/' }}</p>
                        </div>
                        <div class="col-lg-6">
                            <label>Total land To</label>
                            <p>{{ $group->total_land_to ? $group->total_land_to : '/' }}</p>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-6">
                            <label>Living Area From</label>
                            <p>{{ $group->living_area_from ? $group->living_area_from : '/' }}</p>
                        </div>
                        <div class="col-lg-6">
                            <label>Living Area To</label>
                            <p>{{ $group->living_area_to ? $group->living_area_to : '/' }}</p>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-12">
                            <h4>Group Profile</h4>
                        </div>
                    </div>
                    
                    <div class="row">
                        <div class="col-lg-6">
                            <label>Maximum number of members</label>
                            <p>{{ $group->max_num_of_members ? $group->max_num_of_members : '/' }}</p>
                        </div>
                        <div class="col-lg-6">
                            <label>Total Capital Requirements</label>
                            <p>{{ $group->total_capital_requirements ? $group->total_capital_requirements : '/' }}</p>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-12">
                            <h4>Group member profiles</h4>
                        </div>
                    </div>
                    
                    <div class="row">
                        <div class="col-lg-6">
                            <label>Gay friendly</label>
                            <p>{{ $group->gay_friendly ? 'Yes' : 'No' }}</p>
                        </div>
                        <div class="col-lg-6">
                            <label>Smoker Tolerant</label>
                            <p>{{ $group->smoker_tolerant ? 'Yes' : 'No' }}</p>
                        </div>
                    </div>      

                    <div class="row">
                        <div class="col-lg-6">
                            <label>Alcohol Tolerant</label>
                            <p>{{ $group->alcohol_tolerant ? 'Yes' : 'No' }}</p>
                        </div>
                        <div class="col-lg-6">
                            <label>Pet Friendly</label>
                            <p>{{ $group->pet_friendly ? 'Yes' : 'No' }}</p>
                        </div>
                    </div>      

                    <div class="row">
                        <div class="col-lg-6">
                            <label>Family Friendly</label>
                            <p>{{ $group->family_friendly ? 'Yes' : 'No' }}</p>
                        </div>
                        <div class="col-lg-6">
                            <label>Disability Accommodating</label>
                            <p>{{ $group->disability_accommodating ? 'Yes' : 'No' }}</p>
                        </div>
                    </div>    

                    <div class="row">
                        <div class="col-lg-6">
                            <label>Age From</label>
                            <p>{{ $group->age_from ? $group->age_from : '/' }}</p>
                        </div>
                        <div class="col-lg-6">
                            <label>Age To</label>
                            <p>{{ $group->age_to ? $group->age_to : '/' }}</p>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-6">
                            <label>Living with</label>
                            <p>{{ $group->living_with ? $group->living_with : '/' }}</p>
                        </div>
                        <div class="col-lg-6">
                            <label>Sexual Orientation</label>
                            <p>{{ $group->sexual_orientation ? $group->sexual_orientation : '/' }}</p>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-6">
                            <label>Nationalities</label>
                            @if(!empty($nationalities))
                                @foreach($nationalities as $n)
                                <p>{{$n['name']}}</p>
                                @endforeach
                            @else
                                <p>/</p>
                            @endif
                        </div>
                        <div class="col-lg-6">
                            <label>Professions</label>
                            @if(!empty($professions))
                                @foreach($professions as $p)
                                <p>{{$p['name']}}</p>
                                @endforeach
                            @else
                                <p>/</p>
                            @endif
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-6">
                            <label>Languages</label>
                            @if(!empty($languages))
                                @foreach($languages as $l)
                                <p>{{$l['name']}}</p>
                                @endforeach
                            @else
                                <p>/</p>
                            @endif
                        </div>
                        <div class="col-lg-6">
                            <label>Religions</label>
                            @if(!empty($religions))
                                @foreach($religions as $r)
                                <p>{{$r['name']}}</p>
                                @endforeach
                            @else
                                <p>/</p>
                            @endif
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-12">
                            <label>Contract</label>
                            <p>{{ $group->contract }}</p>
                        </div>
                    </div>

                    <div class="form-group">
                        <a href="{{ url('group/edit/'. $group->id ) }}" class="site-btn c-btn">Edit</a>
                    </div>

                </div>
            </div>
        </div>
    </div>
</section>
@endsection
