@extends('layouts.main')

@section('main_content')

<script type="text/javascript">
    $( document ).ready(function() {
        $( "#type" ).change(function() {
            var url = '';
            if($(this).val() == "property"){
                url = "https://www.share-realestate.com/search_property";
            }else if($(this).val() == "group"){
                url = "https://www.share-realestate.com/search_group";
            }else if($(this).val() == "provider"){
                url = "https://www.share-realestate.com/search_provider";
            }
            window.location.href = url;
        });

        $('#slider-range .slider-left').text($('#price').val() + 'k');
        $( "#slider-range" ).slider( "values", 0, $('#price').val());
    });
</script>

<section class="hero-section set-bg search-result" data-setbg="img/bg.jpg">
    <div class="container hero-text text-white">
        <h2>Search @if($type == "property"){{ 'Properties' }}@elseif($type == "group"){{ "Groups" }}@else{{ "Service Providers" }}@endif</h2>
    </div>
</section>

<!-- Filter Search Section Begin -->
<div class="filter-search">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <form id="search_form" class="filter-form @if($type == 'property'){{'search_property'}}@elseif($type == 'group'){{'search_group'}}@else{{'search_provider'}}@endif" method="POST" action="@if($type == 'property'){{'/search_property'}}@elseif($type == 'group'){{'/search_group'}}@else{{'/search_provider'}}@endif">
                    {{ csrf_field() }}
                    <div class="search-type">
                        <p>Type</p>
                        <select id="type" class="type filter-property" name="type">
                            <option value="property" @if($type == "property") {{ 'selected' }} @endif>Property</option>
                            <option value="group" @if($type == "group") {{ 'selected' }} @endif>Group</option>
                            <option value="provider" @if($type == "provider") {{ 'selected' }} @endif>Service Provider</option>                         
                        </select>
                    </div>
                    <div class="location">
                        <p>Location</p>
                        <select class="country filter-location" name="country" id="country">
                            <option value="">Country</option>
                            <?php foreach ($countries as $key => $value) { ?>
                                <option value="<?php echo $value->id; ?>" <?php if(isset($country) && $country == $value->id) echo 'selected'; ?>><?php echo $value->name; ?></option>
                            <?php }?>                         
                        </select>
                    </div>
                    @if($type == "property" || $type == "group")
                    <div class="search-type">
                        <p>Category</p>
                        <select class="category filter-property" name="category" id="category">
                            <option value="">Category</option>
                            <?php foreach ($property_types as $key => $value) { ?>
                                <option value="<?php echo $value->id; ?>" <?php if(isset($category) && $category == $value->id) echo 'selected'; ?>><?php echo $value->name; ?></option>
                            <?php }?>
                        </select>
                    </div>
                    @endif
                    @if($type == "property")
                    <div class="price-range">
                        <p>Price</p>
                        <input type="hidden" value="@if(isset($price) && $price != null){{ $price }}@else{{ 0 }}@endif" name="price" id="price">
                        <div class="price range-slider">
                            <div id="slider-range">
                                <div tabindex="0"
                                    class="ui-slider-handle ui-corner-all ui-state-default slider-left">0k</div>
                                <div tabindex="0"
                                    class="ui-slider-handle ui-corner-all ui-state-default slider-right">1000k</div>
                            </div>
                        </div>
                    </div>
                    <div class="bedrooms">
                        <p>Bedrooms</p>
                        <div class="room-filter-pagi">
                            <div class="bf-item">
                                <input type="radio" name="room[]" value="1" id="room-1" @if(isset($room) && in_array(1, $room)) {{'checked=true'}} @endif>
                                <label for="room-1">1</label>
                            </div>
                            <div class="bf-item">
                                <input type="radio" name="room[]" value="2" id="room-2" @if(isset($room) && in_array(2, $room)) {{'checked=true'}} @endif>
                                <label for="room-2">2</label>
                            </div>
                            <div class="bf-item">
                                <input type="radio" name="room[]" value="3" id="room-3" @if(isset($room) && in_array(3, $room)) {{'checked=true'}} @endif>
                                <label for="room-3">3</label>
                            </div>
                            <div class="bf-item">
                                <input type="radio" name="room[]" value="4" id="room-4" @if(isset($room) && in_array(4, $room)) {{'checked=true'}} @endif>
                                <label for="room-4">4+</label>
                            </div>
                        </div>
                    </div>
                    @endif
                    @if($type == "group")
                    <div class="location">
                        <p>Use of Property</p>
                        <select class="use_of_property filter-location" name="use_of_property">
                            <option value="">Use of Property</option>
                            <option value="co_living_purchase" @if(isset($use_of_property) && $use_of_property == "co_living_purchase") {{ 'selected' }} @endif>Co-living (purchase)</option>
                            <option value="co_living_rental" @if(isset($use_of_property) && $use_of_property == "co_living_rental") {{ 'selected' }} @endif>Co-living (rental)</option>
                            <option value="co_working_purchase" @if(isset($use_of_property) && $use_of_property == "co_working_purchase") {{ 'selected' }} @endif>Co-working (purchase)</option>
                            <option value="co_working_rental" @if(isset($use_of_property) && $use_of_property == "co_working_rental") {{ 'selected' }} @endif>Co-working (rental)</option>
                            <option value="shared_vacation_property" @if(isset($use_of_property) && $use_of_property == "shared_vacation_property") {{ 'selected' }} @endif>Shared vacation property (purchase)</option>
                            <option value="investment_property" @if(isset($use_of_property) && $use_of_property == "investment_property") {{ 'selected' }} @endif>Investment property</option>
                            <option value="business_venture_purchase" @if(isset($use_of_property) && $use_of_property == "business_venture_purchase") {{ 'selected' }} @endif>Business venture (purchase)</option>
                            <option value="business_venture_rental" @if(isset($use_of_property) && $use_of_property == "business_venture_rental") {{ 'selected' }} @endif>Business venture (rental)</option>
                            <option value="other">Other</option>
                        </select>
                    </div>
                    <div class="search-contract">
                        <p>Contract</p>
                        <select id="contract" class="type filter-property" name="contract">
                            <option value="">Contract</option>
                            <option value="residentional" @if(isset($contract) && $contract == "residentional") {{ 'selected' }} @endif>Residentional</option>
                            <option value="vacation" @if(isset($contract) && $contract == "vacation") {{ 'selected' }} @endif>Vacation</option>
                            <option value="investment" @if(isset($contract) && $contract == "investment") {{ 'selected' }} @endif>Investment</option>                         
                        </select>
                    </div>
                    @endif
                    @if($type == "provider")
                    <div class="location">
                        <p>Type</p>
                        <select name="sp_type" class="filter-location">
                            <option value="">Type</option>
                            @foreach($types as $service_provider_type)
                                <option value="{{ $service_provider_type->id }}" <?php if(isset($sp_type) && $sp_type == $service_provider_type->id) echo 'selected'; ?>>{{ $service_provider_type->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="location company-name">
                        <p class="company-name-label">Company Name</p>
                        <input type="text" placeholder="Company Name" class="form-control" name="company_name" value="@if(isset($company_name)){{$company_name}}@endif" />
                    </div>
                    @endif
                    <div class="search-btn">
                        <button type="submit"><i class="flaticon-search"></i>Search</button>
                    </div>
                    <!-- submit -->
                </form>
            </div>
        </div>
    </div>
</div>
@if($type == "property")
    @include('search.properties')
@elseif($type == "group")
    @include('search.groups')
@else
    @include('search.providers')
@endif

@endsection