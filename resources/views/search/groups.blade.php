<section class="hotel-rooms spad-2">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 p-45">
                <div class="found-items">
                    <h4>We found <span>{{count($results)}}</span> @if(count($results) == 1){{'group'}}@else{{'groups'}}@endif</h4>
                    <!-- <select class="date-select">
                        <option value="0">Date New to Old</option>
                        <option value="1">Old</option>
                        <option value="2">New</option>
                    </select> -->
                </div>
            </div>
        </div>
        <div class="row">
        	@if(count($results) > 0)
        	@foreach ($results as $result)
            <div class="col-lg-4 col-md-4 col-md-6">
                <div class="room-items">
                    <div class="room-text">
                        <div class="room-details">
                            <div class="room-title">
                                <h5>{{ $result->group_name}}</h5>
                                <a href="#"><i class="flaticon-placeholder"></i> <span>{{ $result->country }}</span></a>
                            </div>
                        </div>
                        <div class="room-features">
                            <div class="room-info">
                                <div class="size">
                                    <p>Category</p>
                                    <span>@if(isset($result->category_name)){{ $result->category_name }}@endif</span>
                                </div>
                                <div class="beds">
                                    <p>Use of Property</p>
                                    <span>@if(isset($result->use_of_property)){{ $result->use_of_property }}@endif</span>
                                </div>
                            </div>
                        </div>

                        <a href="{{ url('single_group/'.$result->url_name) }}" class="site-btn btn-line join_grop">Join Group</a>
                        <a href="{{ url('single_group/'.$result->url_name) }}" class="site-btn btn-line view_group">View Group</a>
                    </div>
                </div>
            </div>
            @endforeach
            @endif
        </div>
    </div>
</section>