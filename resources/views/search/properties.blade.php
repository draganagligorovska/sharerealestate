<section class="hotel-rooms spad-2">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 p-45">
                <div class="found-items">
                    <h4>We found <span>{{count($results)}}</span> @if(count($results) == 1){{'service provider'}}@else{{'service providers'}}@endif</h4>
                </div>
            </div>
        </div>
        <div class="row">
        	@if(count($results) > 0)
        	@foreach ($results as $result)
            <div class="col-lg-4 col-md-4 col-md-6">
                <div class="room-items">
                    <div class="room-img set-bg" data-setbg="@if($result->image != null){{'/images/photo_gallery/properties/'.$result->image}}@else{{'/images/default_property.jpg'}}@endif">
                    </div>
                    <div class="room-text">
                        <div class="room-details">
                            <div class="room-title">
                                <h5>{{ $result->title}}</h5>
                                <a href="#"><i class="flaticon-placeholder"></i> <span>{{ $result->country }}</span></a>
                            </div>
                        </div>
                        <div class="room-features">
                            <div class="room-info">
                                <div class="size">
                                    <p>Lot Size</p>
                                    <img src="img/rooms/size.png" alt="">
                                    <i class="flaticon-bath"></i>
                                    <span>@if(isset($result->total_land)){{ $result->total_land }}@endif</span>
                                </div>
                                <div class="beds">
                                    <p>Bedrooms</p>
                                    <img src="img/rooms/bed.png" alt="">
                                    <span>@if(isset($result->number_of_bedrooms)){{ $result->number_of_bedrooms }}@endif</span>
                                </div>
                                <div class="baths">
                                    <p>Pool</p>
                                    <img src="img/rooms/bath.png" alt="">
                                    <span>@if(isset($result->pool) && $result->pool == 1){{"Yes"}}@else{{ "No" }}@endif</span>
                                </div>
                                <!-- <div class="garage">
                                    <p>Garage</p>
                                    <img src="img/rooms/garage.png" alt="">
                                    <span>1</span>
                                </div> -->
                            </div>
                        </div>
                        <div class="room-price">
                            <p>For Sale</p>
                            <span>{{ $result->price }}EUR</span>
                        </div>
                        <a href="{{ url('single_property/'.$result->url_title) }}" class="site-btn btn-line">View Property</a>
                    </div>
                </div>
            </div>
            @endforeach
            @endif
        </div>
    </div>
</section>