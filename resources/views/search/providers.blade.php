<section class="hotel-rooms spad-2">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 p-45">
                <div class="found-items">
                    <h4>We found <span>{{count($results)}}</span> @if(count($results) == 1){{'service provider'}}@else{{'service providers'}}@endif</h4>
                </div>
            </div>
        </div>
        <div class="row">
        	@if(count($results) > 0)
        	@foreach ($results as $result)
            <div class="col-lg-4 col-md-4 col-md-6">
                <div class="room-items">
                    <div class="room-img set-bg" data-setbg="{{ asset('images/default_provider_profile.png')}}">
                    </div>
                    <div class="room-text">
                        <div class="room-details">
                            <div class="room-title">
                                <h5>{{ $result->first_name .' '.$result->last_name}}</h5>
                                <a href="#"><i class="flaticon-placeholder"></i> <span>@if($result->country_name != null){{ $result->country_name }}@else{{'No location'}}@endif</span></a>
                            </div>
                        </div>
                        <div class="room-features">
                            <div class="room-info">
                                <div class="size">
                                    <p>Type</p>
                                    <span>@if(isset($result->category_name)){{ $result->category_name }}@else{{'/'}}@endif</span>
                                </div>
                            </div>
                        </div>

                        <a href="{{ url('single_group/'.$result->url_name) }}" class="site-btn btn-line view_group">View Service Provider</a>
                    </div>
                </div>
            </div>
            @endforeach
            @endif
        </div>
    </div>
</section>