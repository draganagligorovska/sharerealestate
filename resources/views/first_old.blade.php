@extends('layouts.main')

@section('main_content')

<section id="main">
    <header>
        @include('layouts.nav')
    </header>
    <footer>
        <h1>Your First Real Estate Service for Shared Properties!</h1>
        <h2>Live beyond your dreams, WITHOUT spending beyon your means!</h2>
    </footer>

</section>

<aside id="sidebar">
        <div class="sidebarInner">
            <input type="button" class="formButton" value="Search Property" id="btn_search" onclick="window.location='{{ url("search_property") }}'">
            <input type="button" class="formButton" value="Search Group" id="btn_search" onclick="window.location='{{ url("search_group") }}'">
            <input type="button" class="formButton" value="Search Provider" id="btn_search" onclick="window.location='{{ url("search_provider") }}'">
            
            <?php if(isset(Auth::user()->type) && Auth::user()->type == 1){ ?>
            <input type="button" class="formButton" value="Invite!" onclick="window.location='{{ url("invite") }}'">
            <?php } ?>
            <!-- <h2>Send invite</h2> -->
            <!-- <form id="getinvite" action="/send_invite" method="POST">
                 @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <input type="text" class="formElement" name="name" placeholder="Name" value="">
                
                <input type="text" class="formElement" name="to" value="" placeholder="Email">
                
                <input type="submit" class="formButton" value="Invite!" name="submit_invite">

                <p class="success_msg"></p>
                <p class="error_msg"></p>
            </form> -->

        </div>

        <div class="sidebarBottom">
            <p><a href="{{ url('/about') }}">About Us</a></p>
            <p><a href="{{ url('/privacy') }}">Privacy & Policy</a></p>
            <p><a href="{{ url('/terms') }}">Terms of Use</a></p>
            <p><a href="{{ url('/faq') }}">FAQ</a></p>
            <p>
                <a href="https://www.facebook.com/ShareRealEstates/" class="fa fa-facebook" target="_blank"></a>
                <a href="https://mobile.twitter.com/EstareReal" class="fa fa-twitter" target="_blank"></a>
                <a href="#" class="fa fa-instagram" target="_blank"></a>
            </p>
            
        </div>
    <div class="clear"></div>
</aside>

<div class="clear"></div>

<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.0/jquery-ui.js"></script>

@endsection