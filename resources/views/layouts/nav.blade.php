<div id="preloder">
    <div class="loader"></div>
</div>
<!-- Header Section Begin -->
<header class="header-section">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="logo">
                    <a href="{{ url('/') }}">
                        <img src="{{ asset('images/logo.png') }}" alt="Share Real Estate Logo">
                    </a>
                </div>
                <ul class="main-menu">
                    @guest
                        <li><a href="{{ url('/search_property') }}">Search</a></li>
                        <li><a href="{{ url('rules') }}">SPV</a></li>
                        <li><a href="{{ url('/about') }}">About Us</a></li>
                        <!-- <li><a href="./blog.html">Blog</a></li> -->
                        <li><a href="{{ url('/contact') }}">Contact</a></li>

                        <li><a href="{{ route('login') }}">Login</a></li>
                        @if (Route::has('register'))
                            <li><a href="{{ route('register') }}">Join ShareRealEstate</a></li>
                        @endif
                    @else
                        <li><a href="{{ url('profile/'. Auth::user()->id ) }}">Profile</a></li>

                        @if(Auth::user()->type == 4)
                            <li><a href="{{ url('search_provider') }}">Search</a></li>
                        @endif

                        @if(Auth::user()->type == 3)
                            <li><a href="{{ url('property/create') }}">Add Property</a></li>
                            <li><a href="{{ url('property/list/'. Auth::user()->id ) }}">My Properties</a></li>
                            <li><a href="{{ url('search_property') }}">Search</a></li>
                        @endif

                        @if(Auth::user()->type == 2)
                            <li><a href="{{ url('rules') }}">SPV</a></li>
                            <li><a href="{{ url('group/create') }}">Create a Group</a></li>
                            <li><a href="{{ url('group/list/'. Auth::user()->id ) }}">My Groups</a></li>
                            <li><a href="{{ url('search_group') }}">Search</a></li>
                        @endif

                        @if(Auth::user()->type == 1)
                            <li><a href="{{ url('search_property') }}">Search</a></li>
                            <li><a href="{{ url('invite') }}">Invite</a></li>
                        @endif

                        <li><a href="{{ url('/about') }}">About Us</a></li>
                        <li><a href="{{ url('/contact') }}">Contact</a></li>

                        <li><a href="{{ route('logout') }}" onclick="event.preventDefault();
                             document.getElementById('logout-form').submit();">{{ __('Logout') }}</a></li>

                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                    @endguest

                    <!-- <li class="top-social">
                        <a href="https://www.facebook.com/ShareRealEstates/" target="_blank"><i class="fa fa-facebook"></i></a>
                        <a href="https://mobile.twitter.com/EstareReal" target="_blank"><i class="fa fa-twitter"></i></a>
                        <a href="https://www.instagram.com/sharerealestat/"><i class="fa fa-instagram" target="_blank"></i></a>
                        <a href="#"><i class="fa fa-linkedin" target="_blank"></i></a>
                    </li> -->
                </ul>
                <div id="mobile-menu-wrap"></div>
            </div>
        </div>
    </div>
</header>
