@extends('layouts.main')
@section('main_content')
 <div id="app">
        @include('layouts.nav')

        <main class="py-4">
            @yield('content')
        </main>
    </div>
@endsection