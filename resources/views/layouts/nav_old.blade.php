<nav class="navbar navbar-expand-md navbar-light navbar-laravel">
    <div class="container">
        <a class="navbar-brand logo" href="{{ url('/') }}">
            <img src="{{ asset('images/logo.png') }}" alt="Share Real Estate Logo">
        </a>
        <!-- <h1>Share Real Estate - Your first service for shared properties</h1> -->
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <!-- Right Side Of Navbar -->
            <ul class="navbar-nav ml-auto">
                @guest
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('login') }}">{{ __('LOGIN') }}</a>
                    </li>
                    @if (Route::has('register'))
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('register') }}">{{ __('JOIN SHAREREALESTATE') }}</a>
                        </li>
                    @endif
                @else
                    <li class="nav-item dropdown">
                        <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                            {{ Auth::user()->first_name .' '.Auth::user()->last_name }} <span class="caret"></span>
                        </a>

                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="{{ url('profile/'. Auth::user()->id ) }}">Profile</a>
                            <!-- Vendor -->
                            <?php if(Auth::user()->type == 1 || Auth::user()->type == 3){ ?>
                            <a class="dropdown-item" href="{{ url('property/create') }}">Add Listing Property</a>
                            <a class="dropdown-item" href="{{ url('property/list/'. Auth::user()->id ) }}">My Properties</a>
                            <?php } ?>

                            <!-- Purchaser -->
                            <?php if(Auth::user()->type == 1 || Auth::user()->type == 2){ ?>
                            <a class="dropdown-item" href="{{ url('group/create') }}">Create a Group</a>
                            <a class="dropdown-item" href="{{ url('search_group') }}">Search & Join an Existing Group</a>
                            <?php } ?>

                            <?php if(Auth::user()->type == 1 || Auth::user()->type == 2 || Auth::user()->type == 3){ ?>
                            <a class="dropdown-item" href="{{ url('providers') }}">Service Providers</a>
                            <?php } ?>

                            <?php if(Auth::user()->type == 1){ ?>
                            <a class="dropdown-item" href="{{ url('invite') }}">Invite</a>
                            <?php } ?>
                            <a class="dropdown-item" href="#">Contracts</a>
                            <a class="dropdown-item" href="{{ route('logout') }}"
                               onclick="event.preventDefault();
                                             document.getElementById('logout-form').submit();">
                                {{ __('Logout') }}
                            </a>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>
                        </div>
                    </li>
                @endguest
            </ul>
        </div>
    </div>
</nav>