@extends('layouts.main')

@section('main_content')
<section class="hero-section set-bg about-us" data-setbg="/img/bg.jpg">
    <div class="container hero-text text-white">
        <h2>Your Profile</h2>
    </div>
</section>
<section class="login-section">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-xl-8">
                <div class="login-form">

                    <h4>{{ __('Your Profile') }}</h4>
                    
                    <div class="row">
                        <div class="col-lg-6">
                            <label>First Name</label>
                            <p>{{ $profile->first_name }}</p>   
                        </div>
                        <div class="col-lg-6">
                            <label>Last Name</label>
                            <p>{{ $profile->last_name }}</p>
                        </div>
                    </div>
                    
                	<div class="row">
                        <div class="col-lg-6">
                            <label>Email</label>
                            <p>{{ $profile->email }}</p>
                        </div>    
                        <div class="col-lg-6">
                            <label>Telephone Number</label>
                            <p>{{ $profile->number ? $profile->number : '/' }}</p>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-12">
                            <label>Country</label>
                            <p>{{ $profile->country_name ? $profile->country_name : '/' }}</p>
                        </div>
                    </div>

                    @if($user_type == 1 || $user_type == 2)
                        @include('profiles.purchasers.preview')
                    @elseif($user_type == 3)
                        @include('profiles.vendors.preview')
                    @elseif($user_type == 4)
                        @include('profiles.service_providers.preview')
                    @endif

                    <div class="row">
                        <div class="col-lg-12">
                            <a href="{{ url('profile/edit/') }}" class="site-btn c-btn">Edit Profile</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
