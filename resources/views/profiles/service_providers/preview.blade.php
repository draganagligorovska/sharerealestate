<div class="row">
    <div class="col-lg-6">
        <label>Type</label>
        @if(!empty($user_types))
            @foreach($user_types as $t)
            <p>{{$t['name']}}</p>
            @endforeach
        @else
            <p>/</p>
        @endif
    </div>
    <div class="col-lg-6">
        <label>Description</label>
        <p>{{ $profile->description ? $profile->description : '/' }}</p>
    </div>
</div>

<div class="row">
    <div class="col-lg-6">
        <label>Company</label>
        <p>{{ $profile->company ? $profile->company : '/' }}</p>
    </div>
    <div class="col-lg-6">
        <label>Company Address</label>
        <p>{{ $profile->company_address ? $profile->company_address : '/' }}</p>
    </div>
</div>