<script type="text/javascript">
    $( document ).ready(function() {
        $('.multiple-select').select2();
    });
</script>
<div class="row">
    <div class="col-lg-12">
        <label>Type</label>
        <select multiple="multiple" name="type_ids[]" class="multiple-select form-control">
            @foreach($types as $id => $value)
                <option value="<?php echo $value->id; ?>" <?php if(in_array($value->id, $user_types)) echo 'selected'; ?>><?php echo $value->name; ?></option>
            @endforeach
        </select>
    </div>
</div>

<div class="row">                
    <div class="col-lg-12">
        <label>Description</label>
        <textarea name="description" class="form-control" placeholder="Description" rows="5">{{ isset($profile->description) ? $profile->description : old('description') }}</textarea>
    </div>
</div>


<div class="row">
    <div class="col-lg-12">
        <label>Company</label>
        <input type="text" value="<?php echo isset($profile->company) ? $profile->company : old('company') ?>" name="company" class="form-control" placeholder="Company Name">    
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <label>Company Address</label>
        <input type="text" value="<?php echo isset($profile->company_address) ? $profile->company_address : old('company_address') ?>" name="company_address" class="form-control" placeholder="Company Address">    
    </div>
</div>