<div class="row">
	<div class="col-lg-6">
	    <label>Company</label>
	    <input type="text" value="<?php echo isset($profile->company) ? $profile->company : old('company') ?>" name="company" class="form-control" placeholder="Company Name">    
	</div>
	<div class="col-lg-6">
	    <label>Company Address</label>
	    <input type="text" value="<?php echo isset($profile->company_address) ? $profile->company_address : old('company_address') ?>" name="company_address" class="form-control" placeholder="Company Address">    
	</div>
</div>
