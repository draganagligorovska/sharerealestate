<div class="row">
    <div class="col-lg-6">
        <label>Company</label>
    	<p>{{ $profile->company ? $profile->company : '/' }}</p>
	</div>
	<div class="col-lg-6">
		<label>Company Address</label>
	    <p>{{ $profile->company_address ? $profile->company_address : '/' }}</p>
    </div>
</div>
