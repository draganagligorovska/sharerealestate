@extends('layouts.main')

@section('main_content')
<!-- Hero Section Begin -->
<section class="hero-section set-bg about-us" data-setbg="/img/bg.jpg">
    <div class="container hero-text text-white">
        <h2>{{ __('Edit Profile') }}</h2>
    </div>
</section>
<!-- Hero Section End -->
<section class="login-section">
    <div class="container">
    <div class="row justify-content-center">
        <div class="col-xl-8">
            <div class="login-form">

                <div class="row">       
                    <div class="col-lg-12">         
                        <h4>{{ __('Edit Profile') }}</h4>
                    </div>
                </div>
                
                <form id="multiselectForm" method="POST" action="/profile/update/{{Auth::user()->id}}">
                    {{ csrf_field() }}

                    @if ($errors->any())
                    <div class="row">
                        <div class="col-lg-12">     
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    </div>
                    @endif
                	<div class="row">
                        <div class="col-lg-6">
                            <label>First Name</label>
                            <input type="text" value="{{ $profile->first_name }}" name="first_name" class="form-control" placeholder="First Name">    
                        </div>
                        <div class="col-lg-6">
                            <label>Last Name</label>
                            <input type="text" value="{{ $profile->last_name }}" name="last_name" class="form-control" placeholder="Last Name">    
                        </div>
                    </div>
                    
                	<div class="row">
                        <div class="col-lg-6">
                            <label>Email</label>
                            <input type="text" value="{{ $profile->email }}" name="email" class="form-control" disabled>    
                        </div>
                        <div class="col-lg-6">
                            <label>Telephone Number</label>
                            <input type="number" value="{{ old('number') ? old('number') : $profile->number }}" name="number" class="form-control" placeholder="Telephone Number">    
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-12">
                            <label>Country</label>
                            <select class="form-control" name="country_id">
                                <option value="">Select Country</option>
                                <?php foreach ($countries as $key => $value) { ?>
                                    <option value="<?php echo $value->id; ?>" <?php if($profile->country_id == $value->id) echo 'selected'; ?>><?php echo $value->name; ?></option>
                                <?php }?>
                            </select>
                        </div>
                    </div>

                    @if($user_type == 1 || $user_type == 2)
                        @include('profiles.purchasers.form')
                    @elseif($user_type == 3)
                        @include('profiles.vendors.form')
                    @elseif($user_type == 4)
                        @include('profiles.service_providers.form')
                    @endif

                    <div class="row">
                        <div class="col-lg-12">
                            <button type="submit" class="site-btn c-btn">Save</button>
                            <button type="button" class="btn btn-default" onclick="window.location='{{ url("profile/". Auth::user()->id) }}'">Cancel</button>
                        </div>
                    </div>
                </form>    
            </div>
        </div>
    </div>
    </div>
</section>
@endsection