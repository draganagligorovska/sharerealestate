@extends('layouts.main')

@section('main_content')
<style>
* {box-sizing: border-box}
body {font-family: Verdana, sans-serif; margin:0}
.mySlides {display: none}
img {vertical-align: middle;}

/* Slideshow container */
.slideshow-container {
  /*max-width: 1000px;*/
  position: relative;
  margin: auto;
}

/* Next & previous buttons */
.prev, .next {
  cursor: pointer;
  position: absolute;
  top: 50%;
  width: auto;
  padding: 16px;
  margin-top: -22px;
  color: white;
  font-weight: bold;
  font-size: 18px;
  transition: 0.6s ease;
  border-radius: 0 3px 3px 0;
  user-select: none;
}

/* Position the "next button" to the right */
.next {
  right: 0;
  border-radius: 3px 0 0 3px;
}

/* On hover, add a black background color with a little bit see-through */
.prev:hover, .next:hover {
  background-color: rgba(0,0,0,0.8);
}

/* Caption text */
.text {
  color: #f2f2f2;
  font-size: 15px;
  padding: 8px 12px;
  position: absolute;
  bottom: 8px;
  width: 100%;
  text-align: center;
}

/* Number text (1/3 etc) */
.numbertext {
  color: #f2f2f2;
  font-size: 12px;
  padding: 8px 12px;
  position: absolute;
  top: 0;
}

/* The dots/bullets/indicators */
.dot {
  cursor: pointer;
  height: 15px;
  width: 15px;
  margin: 0 2px;
  background-color: #bbb;
  border-radius: 50%;
  display: inline-block;
  transition: background-color 0.6s ease;
}

.active, .dot:hover {
  background-color: #717171;
}

/* Fading animation */
.fade {
  -webkit-animation-name: fade;
  -webkit-animation-duration: 1.5s;
  animation-name: fade;
  animation-duration: 1.5s;
}

@-webkit-keyframes fade {
  from {opacity: .4} 
  to {opacity: 1}
}

@keyframes fade {
  from {opacity: .4} 
  to {opacity: 1}
}

/* On smaller screens, decrease text size */
@media only screen and (max-width: 300px) {
  .prev, .next,.text {font-size: 11px}
}
</style>

<div class="slideshow-container">

    <div class="mySlides fade">
      <!-- <div class="numbertext">1 / 3</div> -->
      <img src="/img/for-co-living-professionals.jpg">
      <div class="text">For Co Living Professionals</div>
    </div>

    <div class="mySlides fade">
      <img src="/img/for-co-working.jpg">
      <div class="text">For Co Working</div>
    </div>

    <div class="mySlides fade">
      <img src="/img/for-everybody.jpg">
      <div class="text">For Everybody</div>
    </div>

    <div class="mySlides fade">
      <img src="/img/for-extraordinary-projects.jpg">
      <div class="text">For Extraordinary Projects</div>
    </div>

    <div class="mySlides fade">
      <img src="/img/for-investment-properties.jpg">
      <div class="text">For Investment Properties</div>
    </div>

    <div class="mySlides fade">
      <img src="/img/for-mixing-economy-lifestyle.jpg">
      <div class="text">For Mixing Economy & Lifestyle</div>
    </div>

    <div class="mySlides fade">
      <img src="/img/for-residential-co-living.jpg">
      <div class="text">For Residential Co Living</div>
    </div>

    <div class="mySlides fade">
      <img src="/img/for-seniors-co-living.jpg">
      <div class="text">For Seniors Co Living</div>
    </div>

    <div class="mySlides fade">
      <img src="/img/for-shared-vacation-property.jpg">
      <div class="text">For Shared Vacation Property</div>
    </div>

    <div class="mySlides fade">
      <img src="/img/for-student-co-living.jpg">
      <div class="text">For Student Co Living</div>
    </div>

    <a class="prev" onclick="plusSlides(-1)">&#10094;</a>
    <a class="next" onclick="plusSlides(1)">&#10095;</a>

</div>
<br>

<div style="text-align:center">
  <span class="dot" onclick="currentSlide(1)"></span> 
  <span class="dot" onclick="currentSlide(2)"></span> 
  <span class="dot" onclick="currentSlide(3)"></span>
  <span class="dot" onclick="currentSlide(4)"></span> 
  <span class="dot" onclick="currentSlide(5)"></span> 
  <span class="dot" onclick="currentSlide(6)"></span>
  <span class="dot" onclick="currentSlide(7)"></span> 
  <span class="dot" onclick="currentSlide(8)"></span> 
  <span class="dot" onclick="currentSlide(9)"></span>
  <span class="dot" onclick="currentSlide(10)"></span> 
</div>

<script>
var slideIndex = 1;
showSlides(slideIndex);

function plusSlides(n) {
  showSlides(slideIndex += n);
}

function currentSlide(n) {
  showSlides(slideIndex = n);
}

function showSlides(n) {
  var i;
  var slides = document.getElementsByClassName("mySlides");
  var dots = document.getElementsByClassName("dot");
  if (n > slides.length) {slideIndex = 1}    
  if (n < 1) {slideIndex = slides.length}
  for (i = 0; i < slides.length; i++) {
      slides[i].style.display = "none";  
  }
  for (i = 0; i < dots.length; i++) {
      dots[i].className = dots[i].className.replace(" active", "");
  }
  slides[slideIndex-1].style.display = "block"; 
  slides[slideIndex-1].style.opacity = 1;  
  dots[slideIndex-1].className += " active";
}
</script>

<!-- Hero Section Begin -->
<!--<section class="hero-section home-page set-bg" data-setbg="img/bg.jpg">
    <div class="container hero-text text-white">
        <!-- <h3>Live beyond your dreams, WITHOUT spending beyon your means!</h3> -->
        <!--<h2>Your First Real Estate Service For Shared Properties!</h2>
    </div>
</section>-->
<!-- Hero Section End -->
<!-- Servies Section Begin -->
<section class="services-section">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="left-side">
                    <h2><span>GETTING STARTED</span></h2>
                    <p>Share Realestate brings people, ideas, and real-estate together to benefit from the “shared economy”.</p>
                    <br/>
                    <p><a href="{{ url('/search_group') }}">SHARED VACATION PROPERTIES</a> directs you to groups which are focussed on purchasing vacation properties. If you want to buy a great vacation property, share the great times, experience, and the costs with others, browse through the groups and see what others are doing.</p>
                    <br/>
                    <p><a href="{{ url('/search_group') }}">CO-WORKING RENTALS</a> and <a href="{{ url('/search_group') }}">CO-WORKING PURCHASES</a> directs you to groups which are focussed on acquiring property to be used as co-working spaces, either for their own use or as a business venture. Thousands of professionals have adopted co-working around the globe. Co-working can be more engaging, interesting, and far more economical. </p>
                    <br />
                    <p><a href="{{ url('/search_group') }}">CO-LIVING RENTALS</a> and <a href="{{ url('/search_group') }}">CO-LIVING PURCHASES</a> directs you to groups focussed on purchasing or renting residential dwellings. Co-living can be fun, economical.is becoming more and more popular. Popular with students since forever, co-living is increasingly popular with professionals and seniors. With the right balance between personal privacy and social life in a group setting, co-living can offer economies of scale permitting you to live large even on a tighter budget.</p>
                    <br/>
                    <p><a href="{{ url('/search_group') }}">INVESTMENT PROPERTY</a> directs you groups focused on business ventures. From small dwellings for student housing to entire apartment blocks, Share Realestate helps property investors form an investment group without an intermediary or taking a large portion of the profits and controlling the decisions.</p>
                    <br/>
                    <p><a href="{{ url('/search_group') }}">OTHER GROUPS</a> directs you to groups which do not fit into our main categories. These groups may be collective projects to buy a yacht, aircraft, or other exciting ventures. </p>
                    <br/>
                    <p><strong>If you don’t find a group which meets your specific needs, or if you have your own idea for a new project</strong>, <a href="{{ url('group/create') }}">CREATE A NEW GROUP</a> ! Define every aspect of the project yourself using your own description of your project, group rules templates, and abundant filters which let you select the type of people you want (and don’t want) to be involved in your project. Lead the project, market your proposition, find the right people, and bring your dream project to fruition. It’s simple, affordable and very rewarding.</p>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Servies Section End -->

<!-- Servies Section End -->
<section class="instagram">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 text-center">
                <h2>Don’t forget to follow us on Instagram @sharerealestat</h2>
            </div>
        </div>
    </div>
</section>

@endsection