@extends('layouts.main')

@section('main_content')
<!-- Hero Section Begin -->
<section class="hero-section set-bg about-us" data-setbg="/img/bg.jpg">
    <div class="container hero-text text-white">
        <h2>{{ __('Reset Password') }}</h2>
    </div>
</section>
<!-- Hero Section End -->
<section class="login-section">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-8">
                <div class="login-form">
                    <h4>{{ __('Reset Password') }}</h4>

                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <form method="POST" action="{{ route('password.email') }}">
                        @csrf

                        <div class="row">
                            <div class="col-lg-12">
                                <input id="email" type="email" placeholder="{{ __('E-Mail Address') }}" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>

                                @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-12">
                                <button type="submit" class="site-btn c-btn">{{ __('Send Password Reset Link') }}</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
