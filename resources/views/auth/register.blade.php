@extends('layouts.main')

@section('main_content')

<!-- Hero Section Begin -->
<section class="hero-section set-bg about-us" data-setbg="img/bg.jpg">
    <div class="container hero-text text-white">
        <h2>{{ __('Register') }}</h2>
    </div>
</section>
<!-- Hero Section End -->

<section class="login-section">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-8">
                <div class="login-form">
                    <h4>{{ __('Register') }}</h4>
                    <form method="POST" action="{{ route('register') }}">
                        @csrf

                        <div class="row">
                            <div class="col-lg-6">
                                <input id="first_name" type="text" placeholder="First Name" class="form-control{{ $errors->has('first_name') ? ' is-invalid' : '' }}" name="first_name" value="{{ old('first_name') }}" required autofocus>

                                @if ($errors->has('first_name'))
                                    <span class="error-message">{{ $errors->first('first_name') }}</span>
                                @endif
                            </div>
                        
                            <div class="col-lg-6">
                                <input id="last_name" type="text" placeholder="Last Name" class="form-control{{ $errors->has('last_name') ? ' is-invalid' : '' }}" name="last_name" value="{{ old('last_name') }}" required autofocus>

                                @if ($errors->has('last_name'))
                                    <span class="error-message">{{ $errors->first('last_name') }}</span>    
                                @endif
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-12">
                                <select id="type" class="form-control{{ $errors->has('type') ? ' is-invalid' : '' }}" name="type">
                                    <option value="">Select User Type</option>
                                    <option value="2" <?php if(old('type') == "2") echo 'selected'; ?>>I want to join an existing group or create a new group</option>
                                    <option value="3" <?php if(old('type') == "3") echo 'selected'; ?>>I am an owner/manger/agent, and want to to list my realestate for sale/rent</option>
                                    <option value="4" <?php if(old('type') == "4") echo 'selected'; ?>>I want to advertise my services</option>
                                </select>

                                @if ($errors->has('type'))
                                    <span class="error-message">{{ $errors->first('type') }}</span>
                                @endif
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-12">
                                <input id="email" type="email" placeholder="Email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>

                                @if ($errors->has('email'))
                                    <span class="error-message">{{ $errors->first('email') }}</span>    
                                @endif
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-12">
                                <input id="password" type="password" placeholder="Password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="error-message">{{ $errors->first('password') }}</span>
                                @endif
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-12">
                                <input id="password-confirm" type="password" placeholder="Confirm Password" class="form-control" name="password_confirmation" required>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-12">
                                <button type="submit" class="site-btn c-btn">
                                    {{ __('Register') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
