@extends('layouts.main')

@section('main_content')
<!-- Hero Section Begin -->
<section class="hero-section set-bg about-us" data-setbg="img/bg.jpg">
    <div class="container hero-text text-white">
        <h2>{{ __('Login') }}</h2>
    </div>
</section>
<!-- Hero Section End -->
<!-- Login Section Start -->
<section class="login-section">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-8">
                <div class="login-form">
                    <h4>{{ __('Login') }}</h4>
                    <form method="POST" action="{{ route('login') }}">
                        @csrf
                        <div class="row">
                            <div class="col-lg-12">
                                <input id="email" type="email" placeholder="Email" class="{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus>
                                @if ($errors->has('email'))
                                    <span class="error-message">{{ $errors->first('email') }}</span>
                                @endif
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <input id="password" type="password" placeholder="Password" class="{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>
                                @if ($errors->has('password'))
                                    <span class="error-message">{{ $errors->first('password') }}</span>
                                @endif
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">                    
                                <input type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                                <label>{{ __('Remember Me') }}</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <button type="submit" class="site-btn c-btn">{{ __('Login') }}</button>

                                @if (Route::has('password.request'))
                                    <a class="btn btn-link" href="{{ route('password.request') }}">
                                        {{ __('Forgot Your Password?') }}
                                    </a>
                                @endif
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Login Section End -->
@endsection
