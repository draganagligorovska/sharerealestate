<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
if(env('APP_ENV') === 'production'){
	\URL::forceScheme('https');
}

Route::get('/', function () {
    // return view('welcome');
    return view('first');
});

Auth::routes();

Route::get('profile/edit/{user_id?}', 'ProfileController@edit');
Route::resource('profile', 'ProfileController');

Route::resource('property', 'PropertyController');

Route::get('property/{property_id}', 'PropertyController@show');
Route::get('property/edit/{property_id}', 'PropertyController@edit');
Route::get('property/list/{user_id}', 'PropertyController@list');
Route::post('property/update/{property_id}', 'PropertyController@update');

Route::get('profile/{user_id}', 'ProfileController@show');
Route::post('profile/update/{user_id}', 'ProfileController@update');

Route::get('/home', 'HomeController@index')->name('home');

Route::resource('group', 'GroupController');

Route::get('group/{group_id}', 'GroupController@show');
Route::get('group/edit/{group_id}', 'GroupController@edit');
Route::get('group/list/{user_id}', 'GroupController@list');
Route::post('group/update/{group_id}', 'GroupController@update');

// Route::get('/group/create', 'GroupController@create')->name('group/create');
// Route::get('/group/join', 'GroupController@join')->name('group/join');
// Route::get('/groups', 'GroupController@index')->name('groups');

Route::get('/about', 'GeneralController@about')->name('about');
Route::get('/contact', 'GeneralController@contact')->name('contact');
Route::get('/privacy', 'GeneralController@privacy')->name('privacy');
Route::get('/faq', 'GeneralController@faq')->name('faq');
Route::get('/terms', 'GeneralController@terms')->name('about');
Route::get('/rules', 'GeneralController@groupRules')->name('rules');

Route::get('/invite', 'GeneralController@invite')->name('invite');
Route::post('/send_invite', 'GeneralController@sendInvite')->name('send_invite');

Route::get('/search_property', 'SearchController@search_property')->name('search_property');
Route::get('/search_group', 'SearchController@search_group')->name('search_group');
Route::get('/search_provider', 'SearchController@search_provider')->name('search_provider');

Route::post('/search_property', 'SearchController@search_results');
Route::post('/search_group', 'SearchController@search_results');
Route::post('/search_provider', 'SearchController@search_results');

Route::get('/single_property/{title}', 'SearchController@single_property');
Route::get('/single_group/{name}', 'SearchController@single_group');
Route::get('/single_provider/{name}', 'SearchController@single_provider');