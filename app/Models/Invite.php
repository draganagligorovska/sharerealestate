<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Invite extends Model
{
    protected $table = 'email_invites';

    protected $fillable = [
      'subject',
      'content',
      'to'
    ];

    public $timestamps = true;
}
