<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Country;
use App\Nationality;
use App\Profession;
use App\Religion;
use App\Language;

class Group extends Model
{
    protected $table = 'groups';

    protected $filable = [
    	'name',
    	'description',
    	'region',
    	'country_id',
    	'type_id',
    	'capital_per_member',
    	'num_of_bedrooms',
    	'balcony',
    	'pool',
    	'parking',
    	'central_heating',
    	'gas',
    	'location_type',
    	'use_of_property',
    	'condition',
    	'year_of_construction_from',
    	'year_of_construction_to',
    	'total_land_from',
    	'total_land_to',
    	'living_area_from',
    	'living_area_to',
    	'max_num_of_members',
    	'total_capital_requirements',
    	'gay_friendly',
    	'smoker_tolerant',
    	'alcohol_tolerant',
    	'pet_friendly',
    	'family_friendly',
    	'disability_accommodating',
    	'age_from',
    	'age_to',
    	'living_with',
    	'sexual_orientation',
        'user_id'
    ];

    public $timestamps = true;

	public function countries()
    {
        return $this->belongsToMany('App\Country');
    }

    public function nationalities()
    {
        return $this->belongsToMany('App\Models\Nationality');
    }

    public function professions()
    {
        return $this->belongsToMany('App\Models\Profession');
    }

    public function religions()
    {
        return $this->belongsToMany('App\Models\Religion');
    }

    public function languages()
    {
        return $this->belongsToMany('App\Models\Language');
    }
}
