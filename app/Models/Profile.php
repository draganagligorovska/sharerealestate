<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Country;
use App\ServiceProviderType;

class Profile extends Model
{
     protected $table = 'profiles';

     protected $fillable = [
      'user_id',
      'number',
      'company',
      'country_id',
      'company_address',
      'description'
      ];

    public $timestamps = true;

    public function countries()
    {
        return $this->belongsToMany('App\Country');
    }

	  public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function service_provider_types()
    {
        return $this->belongsToMany('App\Models\ServiceProviderType');
    }
}
