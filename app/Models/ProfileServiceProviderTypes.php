<?php

namespace App\Models;
use App\ServiceProviderType;
use App\Profile;

use Illuminate\Database\Eloquent\Model;

class ProfileServiceProviderTypes extends Model
{
    protected $table = 'profile_service_provider_type';

    protected $fillable = [
      'profile_id',
      'type_id'
    ];
}
