<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Country;

class Property extends Model
{
    protected $table = 'properties';

    protected $fillable = ['title',
      'description',
      'image',
      'price',
      'type',
      'country_id',
      'address',
      'year_of_construction',
      'number_of_bedrooms',
      'total_land',
      'living_area',
      'situation',
      'balcony',
      'pool',
      'parking',
      'central_heating',
      'gas',
      'contact_name',
      'contact_telephone',
      'contact_email',
      'promotion_period_years',
      'promotion_period_months',
      'user_id'];

    public $timestamps = true;

    public function countries()
    {
        return $this->belongsToMany('App\Country');
    }
}
