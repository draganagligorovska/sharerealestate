<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Group;

class Profession extends Model
{
	protected $table = 'professions';

    protected $fillable = [
      'name'
    ];

	public function groups()
    {
        return $this->belongsToMany('App\Models\Group');
    }
}