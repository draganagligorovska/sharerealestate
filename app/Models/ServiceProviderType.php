<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Profile;

class ServiceProviderType extends Model
{
    
    public function profiles()
    {
        return $this->belongsToMany('App\Profile');
    }
}
