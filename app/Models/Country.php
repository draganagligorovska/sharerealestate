<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    public function properties()
    {
        return $this->belongsToMany('App\Property');
    }
}