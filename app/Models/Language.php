<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Group;

class Language extends Model
{
	protected $table = 'languages';

    protected $fillable = [
      'code',
      'name'
    ];

	public function groups()
    {
        return $this->belongsToMany('App\Models\Group');
    }
}