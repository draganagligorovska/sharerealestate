<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Group;

class Religion extends Model
{
	protected $table = 'religions';

    protected $fillable = [
      'name'
    ];

	public function groups()
    {
        return $this->belongsToMany('App\Models\Group');
    }
}