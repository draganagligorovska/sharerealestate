<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Group;

class Nationality extends Model
{
	protected $table = 'nationalities';

    protected $fillable = [
      'name'
    ];

	public function groups()
    {
        return $this->belongsToMany('App\Models\Group');
    }
}