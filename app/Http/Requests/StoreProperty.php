<?php

namespace App\Http\Requests;

use App\Models\Property;
use Illuminate\Foundation\Http\FormRequest;

class StoreProperty extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required',
            'description' => 'required',
            'price' => 'required|numeric',
            'type' => 'required',
            'country_id' => 'required',
            'address' => 'required',
            'year_of_construction' => 'required',
            'number_of_bedrooms' => 'required',
            'total_land' => 'required',
            'living_area' => 'required',
            'situation' => 'required',
            'promotion_period_years' => 'required',
            'promotion_period_months' => 'required'
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'country_id.required' => 'The country field is required'
        ];
    }
}
