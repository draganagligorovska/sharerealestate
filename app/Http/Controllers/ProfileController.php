<?php

namespace App\Http\Controllers;

use App\Models\Country;
use App\Models\ServiceProviderType;
use App\Models\Profile;
use App\User;
use Illuminate\Http\Request;

use App\Http\Requests\StoreProfile;

class ProfileController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Profile  $profile
     * @return \Illuminate\Http\Response
     */
    public function show($user_id)
    {   
        $profile = Profile::join('users', 'profiles.user_id', '=', 'users.id')
            ->leftJoin('countries', 'profiles.country_id', '=', 'countries.id')
            ->select('users.first_name', 'users.last_name', 'users.type as user_type', 
                     'users.email', 'countries.name as country_name', 'profiles.id as profile_id',
                     'profiles.number', 'profiles.company', 'profiles.company_address',
                     'profiles.country_id', 'profiles.description')
            ->where('users.id', $user_id)
            ->get()->first();

        $user_types = array();
        if($profile->user_type == 4){
            $p = Profile::find($profile->profile_id);
            $user_types = $p->service_provider_types()->get()->toArray();
        }
        
        return view('profiles/preview')
            ->with('profile', $profile)
            ->with('user_types', $user_types)
            ->with('user_type', $profile->user_type);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Profile  $profile
     * @return \Illuminate\Http\Response
     */
    public function edit($user_id = null)
    {
        if (is_null($user_id)) { //set id of currently signed in user if id == null
            $user_id = auth()->user()->id; 
        }
        $countries = Country::all();
        $profile = Profile::join('users', 'profiles.user_id', '=', 'users.id')
                ->leftJoin('countries', 'profiles.country_id', '=', 'countries.id')
                ->select('users.first_name', 'users.last_name', 'users.email', 
                    'countries.name as country_name', 'profiles.id as profile_id',
                    'profiles.number', 'profiles.company', 'profiles.company_address', 
                    'profiles.country_id', 'profiles.description')
                ->where('users.id', $user_id)
                ->get()
                ->first();

        $user = User::find($user_id);

        if($user->type == 4){
            $types = ServiceProviderType::all();
            $p = Profile::find($profile->profile_id);
            $user_types = $p->service_provider_types()->get();
            $ut = array_map(function($t) {
                return $t['id'];
            }, $user_types->toArray());
        
            $profile_types = array();
            return view('profiles/form')
                            ->with('countries', $countries)
                            ->with('profile', $profile)
                            ->with('types', $types)
                            ->with('user_types', $ut)
                            ->with('profile_types', $profile_types)
                            ->with('user_type', $user->type);     
        }else{
            return view('profiles/form')
                            ->with('countries', $countries)
                            ->with('profile', $profile)
                            ->with('user_type', $user->type);
        }
       
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Profile  $profile
     * @return \Illuminate\Http\Response
     */
    public function update(StoreProfile $request, $user_id)
    {
        $validated = $request->validated();
        
        $data = $request->all();

        $user = User::findOrFail($user_id);
        $user->first_name = $data['first_name'];
        $user->last_name = $data['last_name'];
        $user->update();

        $profile = Profile::where('user_id', $user_id)->firstOrFail();
        $profile->company = isset($data['company']) ? $data['company'] : null;
        $profile->company_address = isset($data['company_address']) ? $data['company_address'] : null;
        $profile->description = isset($data['description']) ? $data['description'] : null;
        $profile->number = $data['number'];
        $profile->country_id = $data['country_id'];    
        
        $profile->update();

        $user = User::find($user_id);
        if($user->type == 4){
            $sync_data = !empty($data['type_ids']) ? $data['type_ids'] : [];
            $profile->service_provider_types()->sync($sync_data);          
        }

        return redirect('profile/'.$user_id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Profile  $profile
     * @return \Illuminate\Http\Response
     */
    public function destroy(Profile $profile)
    {
        //
    }
}
