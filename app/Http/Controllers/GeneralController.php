<?php

namespace App\Http\Controllers;

use App\Http\Requests\SendInvite;
use App\Models\Invite;
// use Illuminate\Http\Request;

// Import PHPMailer classes into the global namespace
// These must be at the top of your script, not inside a function
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

// Load Composer's autoloader
// require 'vendor/autoload.php';

class GeneralController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    public function about()
    {
        return view('general/about');
    }

    public function privacy()
    {
        return view('general/privacy');
    }

    public function faq()
    {
        return view('general/faq');
    }

    public function terms()
    {
        return view('general/terms');
    }

    public function groupRules()
    {
        return view('general/group_rules');
    }

    public function invite()
    {
        return view('invite');
    }

    public function contact()
    {
        return view('general/contact');
    }

    public function sendInvite(SendInvite $request){

        // Instantiation and passing `true` enables exceptions
        $mail = new PHPMailer(true);

        $validated = $request->validated();

        $data = $request->all();

        try {
            //Server settings
            $mail->SMTPDebug = 2;                                       // Enable verbose debug output
            $mail->isSMTP();                                            // Set mailer to use SMTP
            $mail->Host       = 'share-realestate.com';                 // Specify main and backup SMTP servers
            $mail->SMTPAuth   = true;                                   // Enable SMTP authentication
            $mail->Username   = 'contact@share-realestate.com';                     // SMTP username
            $mail->Password   = 'X2MEoC6JPjib';                               // SMTP password
            $mail->SMTPSecure = 'tls';                                  // Enable TLS encryption, `ssl` also accepted
            $mail->Port       = 587;                                    // TCP port to connect to

            //Recipients
            $mail->setFrom('contact@share-realestate.com', 'Share Real Estate');
            // $mail->addAddress('draganagligorovska@live.com', 'Joe User');     // Add a recipient
            $mail->addAddress($data['to']);                                      // Name is optional
            $mail->addReplyTo('contact@share-realestate.com', 'Information');
            // $mail->addCC('cc@example.com');
            // $mail->addBCC('bcc@example.com');

            // Attachments
            // $mail->addAttachment('/var/tmp/file.tar.gz');         // Add attachments
            // $mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name

            // Content
            $mail->isHTML(false);                                  // Set email format to HTML
            $mail->Subject = $data['subject'];
            $mail->Body    = $data['content'];
            // $mail->AltBody = $data['content'];

            $sent = $mail->send();
            if($sent){
                $invite = new Invite();
                $invite->subject = $data['subject'];
                $invite->content = $data['content'];
                $invite->to = $data['to'];
                $invite->save();

                return redirect('invite');
                // $emessage = 'The email was send!';
                // return view('invite')->with('message', $emessage);
            }else{
                $error_msg = 'There was a problem, the email was not send!';
                return view('invite')->with('error_msg', $error_msg);
            }
        } catch (Exception $e) {
            $error_msg = 'There was a problem, the email was not send!';
            return view('invite')->with('error_msg', $error_msg);
        }

        // $validated = $request->validated();

        // $data = $request->all();
        // $to      = $data['to'];
        // $subject = $data['subject'];
        // $message = $data['content'];
        // $headers = 'From: sharerea@share-realestate.com' . "\r\n" .
        //     'Reply-To: sharerea@share-realestate.com' . "\r\n" .
        //     'X-Mailer: PHP/' . phpversion();

        // $sent = mail($to, $subject, $message, $headers);

        // if($sent){
        //     $invite = new Invite();
        //     $invite->subject = $data['subject'];
        //     $invite->content = $data['content'];
        //     $invite->to = $data['to'];
        //     $invite->save();

        //     return redirect('invite');
        // }else{
        //     $error_msg = 'There was a problem, the email was not send!';
        //     return view('invite')->with('error_msg', $error_msg);
        // }
        
    }
}
