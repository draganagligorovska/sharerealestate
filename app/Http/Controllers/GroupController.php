<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreGroup;

use App\Models\Group;
use App\Models\Country;
use App\Models\Religion;
use App\Models\Nationality;
use App\Models\Profession;
use App\Models\PropertyType;
use App\Models\Language;

use Illuminate\Support\Facades\DB;

class GroupController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('groups/list');
    }

    public function create()
    {
        $countries = Country::all(); 
        $religions = Religion::all(); 
        $nationalities = Nationality::all();
        $professions = Profession::all();
        $property_types = PropertyType::all();
        $languages = Language::all();

        return view('groups/form')
                ->with('countries', $countries)
                ->with('property_types', $property_types)
                ->with('religions', $religions)
                ->with('professions', $professions)
                ->with('nationalities', $nationalities)
                ->with('languages', $languages)
                ;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreGroup $request)
    {
        $validated = $request->validated();
        $group = new Group();

        $data = $request->all();

        $group->balcony = isset($data['balcony']) ? 1 : 0; 
        $group->pool = isset($data['pool']) ? 1 : 0; 
        $group->parking = isset($data['parking']) ? 1 : 0; 
        $group->central_heating = isset($data['central_heating']) ? 1 : 0; 
        $group->gas = isset($data['gas']) ? 1 : 0;
        $group->gay_friendly = isset($data['gay_friendly']) ? 1 : 0; 
        $group->smoker_tolerant = isset($data['smoker_tolerant']) ? 1 : 0; 
        $group->alcohol_tolerant = isset($data['alcohol_tolerant']) ? 1 : 0; 
        $group->pet_friendly = isset($data['pet_friendly']) ? 1 : 0; 
        $group->family_friendly = isset($data['family_friendly']) ? 1 : 0; 
        $group->disability_accommodating = isset($data['disability_accommodating']) ? 1 : 0; 

        $group->name = $data['name'];
        $group->description = $data['description'];
        $group->region = $data['region'];
        $group->country_id = $data['country_id'];
        $group->type_id = $data['category'];
        $group->capital_per_member = $data['capital_per_member'];
        $group->num_of_bedrooms = $data['num_of_bedrooms'];

        $group->location_type = $data['situation'];
        $group->use_of_property = $data['use_of_property'];
        $group->condition = $data['condition'];

        $group->year_of_construction_from = $data['year_of_construction_from'];
        $group->year_of_construction_to = $data['year_of_construction_to'];

        $group->total_land_from = $data['total_land_from'];
        $group->total_land_to = $data['total_land_to'];

        $group->living_area_from = $data['living_area_from'];
        $group->living_area_to = $data['living_area_to'];

        $group->max_num_of_members = $data['max_num_of_members'];
        $group->total_capital_requirements = $data['total_capital_requirements'];

        $group->age_from = $data['age_from'];
        $group->age_to = $data['age_to'];
        $group->living_with = $data['living_with'];
        $group->sexual_orientation = $data['sexual_orientation'];
        $group->contract = $data['contract'];

        $group->user_id = \Auth::user()->id;

        if(empty($error)) $group->save();
        else return redirect()->back()->with('group',$group);

        $sync_data_nationalities = !empty($data['nationality_ids']) ? $data['nationality_ids'] : [];
        $group->nationalities()->sync($sync_data_nationalities);

        $sync_data_professions = !empty($data['profession_ids']) ? $data['profession_ids'] : [];
        $group->professions()->sync($sync_data_professions);

        $sync_data_religions = !empty($data['religion_ids']) ? $data['religion_ids'] : [];
        $group->religions()->sync($sync_data_religions);

        $sync_data_languages = !empty($data['language_ids']) ? $data['language_ids'] : [];
        $group->languages()->sync($sync_data_languages);
        
        return redirect('group/'.$group->id);
    }

     /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $group = DB::table('groups')
            // ->join('users', 'groups.user_id', '=', 'users.id')
            ->leftJoin('countries', 'groups.country_id', '=', 'countries.id')
            ->leftJoin('property_types', 'groups.type_id', '=', 'property_types.id')
            ->select('countries.name as country_name', 'groups.*', 'property_types.name as type_name')
            ->where('groups.id', $id)
            ->get()->first();

        $group->capital_per_member = implode(' ', explode('_', $group->capital_per_member));
        $group->total_capital_requirements = implode(' ', explode('_', $group->total_capital_requirements));
        $group->use_of_property = implode(' ', explode('_', $group->use_of_property));
        $group->condition = implode(' ', explode('_', $group->condition));

        $g = Group::find($id);
        $nationalities = $g->nationalities()->get()->toArray();
        $professions = $g->professions()->get()->toArray();
        $languages = $g->languages()->get()->toArray();
        $religions = $g->religions()->get()->toArray();

        return view('groups/show')
            ->with('group', $group)
            ->with('nationalities', $nationalities)
            ->with('professions', $professions)
            ->with('languages', $languages)
            ->with('religions', $religions);
    }

    public function join()
    {
        return view('groups/join_group');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Group  $group
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $countries = Country::all(); 
        $religions = Religion::all(); 
        $nationalities = Nationality::all();
        $professions = Profession::all();
        $property_types = PropertyType::all();
        $languages = Language::all();

        $group = DB::table('groups')
            ->join('users', 'groups.user_id', '=', 'users.id')
            ->leftJoin('property_types', 'groups.type_id', '=', 'property_types.id')
            ->select('groups.*','property_types.name as type_name')
            ->where('groups.id', $id)
            ->get()->first();

        $g = Group::find($id);
        $nationality_ids = $g->nationalities()->pluck('nationality_id')->toArray();
        $profession_ids = $g->professions()->pluck('profession_id')->toArray();
        $language_ids = $g->languages()->pluck('language_id')->toArray();
        $religion_ids = $g->religions()->pluck('religion_id')->toArray();
       
        return view('groups/form')
                ->with('group', $group)
                ->with('countries', $countries)
                ->with('property_types', $property_types)
                ->with('religions', $religions)
                ->with('professions', $professions)
                ->with('nationalities', $nationalities)
                ->with('languages', $languages)
                ->with('religion_ids', $religion_ids)
                ->with('nationality_ids', $nationality_ids)
                ->with('language_ids', $language_ids)
                ->with('profession_ids', $profession_ids)
                ;
    }

    public function update(StoreGroup $request, $id)
    {
        $validated = $request->validated();
        
        $group = Group::find($id);

        $data = $request->all();

        $group->balcony = isset($data['balcony']) ? 1 : 0; 
        $group->pool = isset($data['pool']) ? 1 : 0; 
        $group->parking = isset($data['parking']) ? 1 : 0; 
        $group->central_heating = isset($data['central_heating']) ? 1 : 0; 
        $group->gas = isset($data['gas']) ? 1 : 0;
        $group->gay_friendly = isset($data['gay_friendly']) ? 1 : 0; 
        $group->smoker_tolerant = isset($data['smoker_tolerant']) ? 1 : 0; 
        $group->alcohol_tolerant = isset($data['alcohol_tolerant']) ? 1 : 0; 
        $group->pet_friendly = isset($data['pet_friendly']) ? 1 : 0; 
        $group->family_friendly = isset($data['family_friendly']) ? 1 : 0; 
        $group->disability_accommodating = isset($data['disability_accommodating']) ? 1 : 0; 

        $group->name = $data['name'];
        $group->description = $data['description'];
        $group->region = $data['region'];
        $group->country_id = $data['country_id'];
        $group->type_id = $data['category'];
        $group->capital_per_member = $data['capital_per_member'];
        $group->num_of_bedrooms = $data['num_of_bedrooms'];

        $group->location_type = $data['situation'];
        $group->use_of_property = $data['use_of_property'];
        $group->condition = $data['condition'];

        $group->year_of_construction_from = $data['year_of_construction_from'];
        $group->year_of_construction_to = $data['year_of_construction_to'];

        $group->total_land_from = $data['total_land_from'];
        $group->total_land_to = $data['total_land_to'];

        $group->living_area_from = $data['living_area_from'];
        $group->living_area_to = $data['living_area_to'];

        $group->max_num_of_members = $data['max_num_of_members'];
        $group->total_capital_requirements = $data['total_capital_requirements'];

        $group->age_from = $data['age_from'];
        $group->age_to = $data['age_to'];
        $group->living_with = $data['living_with'];
        $group->sexual_orientation = $data['sexual_orientation'];
        $group->contract = $data['contract'];

        if(empty($error)) $group->save();
        else return redirect()->back()->with('group',$group);

        $sync_data_nationalities = !empty($data['nationality_ids']) ? $data['nationality_ids'] : [];
        $group->nationalities()->sync($sync_data_nationalities);

        $sync_data_professions = !empty($data['profession_ids']) ? $data['profession_ids'] : [];
        $group->professions()->sync($sync_data_professions);

        $sync_data_religions = !empty($data['religion_ids']) ? $data['religion_ids'] : [];
        $group->religions()->sync($sync_data_religions);

        $sync_data_languages = !empty($data['language_ids']) ? $data['language_ids'] : [];
        $group->languages()->sync($sync_data_languages);
        
        return redirect('group/'.$group->id);
    }


    public function list($user_id)
    {
        $groups = DB::table('groups')
            ->select('groups.id', 'groups.name')
            ->join('users', 'groups.user_id', '=', 'users.id')
            ->where('groups.user_id', $user_id)
            ->orderBy('id', 'desc')
            ->get();
        return view('groups/list')->with('groups', $groups);
    }
}
