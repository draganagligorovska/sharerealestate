<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreProperty;

use App\Models\Country;
use App\Models\Property;
use App\Models\PropertyType;
// use Illuminate\Contracts\Routing\ResponseFactory;

use Illuminate\Support\Facades\DB;

// use Illuminate\Http\Request;

class PropertyController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $countries = Country::all(); // got this from database model
        $property_types = PropertyType::all();
    
        return view('properties/form')->with('countries', $countries)->with('property_types', $property_types);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreProperty $request)
    {
        $validated = $request->validated();
        $property = new Property();

        $data = $request->all();

        $property->title = $data['title']; 
        $property->description = $data['description'];
        $property->price = $data['price']; 
        $property->type = $data['type']; 
        $property->country_id = $data['country_id']; 
        $property->address = $data['address']; 
        $property->year_of_construction = $data['year_of_construction']; 
        $property->number_of_bedrooms = $data['number_of_bedrooms']; 
        $property->total_land = $data['total_land']; 
        $property->living_area = $data['living_area']; 
        $property->situation = $data['situation']; 

        $property->balcony = isset($data['balcony']) ? 1 : 0; 
        $property->pool = isset($data['pool']) ? 1 : 0;
        $property->parking = isset($data['parking']) ? 1 : 0; 
        $property->central_heating = isset($data['central_heating']) ? 1 : 0; 
        $property->gas = isset($data['gas']) ? 1 : 0;

        $property->promotion_period_years = $data['promotion_period_years']; 
        $property->promotion_period_months = $data['promotion_period_months']; 

        //upload images
        $extension=array("jpeg","jpg","png");
        $error=array();
        $property->images = '';
        foreach($_FILES["files"]["tmp_name"] as $key=>$tmp_name) {
            if($tmp_name != ""){
                $file_name=$_FILES["files"]["name"][$key];
                $file_tmp=$_FILES["files"]["tmp_name"][$key];
                $ext=pathinfo($file_name,PATHINFO_EXTENSION);

                if(in_array($ext,$extension)) {
                    $filename=basename($file_name,$ext);
                    $filename = str_replace(" ","_",$filename);
                    $newFileName=$filename.time().".".$ext;
                    move_uploaded_file($file_tmp=$_FILES["files"]["tmp_name"][$key],"images/photo_gallery/properties/".$newFileName);
                
                    if($property->images != ""){
                        $property->images .= ";";
                    }
                    $property->images .= $newFileName;
                }
                else {
                    array_push($error,"$file_name, format is invalid. Only .jpeg, .jpg and .png files allowed!");
                }
            }
        }

        $property->user_id = \Auth::user()->id;

        if(empty($error)) $property->save();
        else return redirect()->back()->withErrors(['image', $error])->with('property',$property);

        return redirect('property/'.$property->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $property = DB::table('properties')
            ->join('users', 'properties.user_id', '=', 'users.id')
            ->leftJoin('countries', 'properties.country_id', '=', 'countries.id')
            ->leftJoin('property_types', 'properties.type', '=', 'property_types.id')
            ->select('countries.name as country_name', 'properties.*', 'property_types.name as type_name')
            ->where('properties.id', $id)
            ->get()->first();

        $property->images = explode(';',$property->images);
        return view('properties/show')->with('property', $property);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $countries = Country::all();
        $property_types = PropertyType::all();
        $property = DB::table('properties')
            ->join('users', 'properties.user_id', '=', 'users.id')
            ->leftJoin('countries', 'properties.country_id', '=', 'countries.id')
            ->leftJoin('property_types', 'properties.type', '=', 'property_types.id')
            ->select('countries.name as country_name', 'properties.*', 'property_types.name as type_name')
            ->where('properties.id', $id)
            ->get()->first();
        $property->images = explode(';', $property->images);
    
        return view('properties/form')
            ->with('property', $property)
            ->with('countries', $countries)
            ->with('property_types', $property_types);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StoreProperty $request, $property_id)
    {
        $validated = $request->validated();
        $property = Property::findOrFail($property_id);
      
        $data = $request->all();

        $property->title = $data['title']; 
        $property->description = $data['description'];
        $property->price = $data['price']; 
        $property->type = $data['type']; 
        $property->country_id = $data['country_id']; 
        $property->address = $data['address']; 
        $property->year_of_construction = $data['year_of_construction']; 
        $property->number_of_bedrooms = $data['number_of_bedrooms']; 
        $property->total_land = $data['total_land']; 
        $property->living_area = $data['living_area']; 
        $property->situation = $data['situation']; 

        $property->balcony = isset($data['balcony']) ? 1 : 0; 
        $property->pool = isset($data['pool']) ? 1 : 0;
        $property->parking = isset($data['parking']) ? 1 : 0; 
        $property->central_heating = isset($data['central_heating']) ? 1 : 0; 
        $property->gas = isset($data['gas']) ? 1 : 0;

        $property->promotion_period_years = $data['promotion_period_years']; 
        $property->promotion_period_months = $data['promotion_period_months']; 

        //upload images
        $extension = array("jpeg","jpg","png");
        $error = array();

        $old_images = $property->images;
        $new_images = '';
        foreach($_FILES["files"]["tmp_name"] as $key => $tmp_name) {
            if($tmp_name != ""){
                $file_name=$_FILES["files"]["name"][$key];
                $file_tmp=$_FILES["files"]["tmp_name"][$key];
                $ext=pathinfo($file_name,PATHINFO_EXTENSION);

                if(in_array($ext,$extension)) {
                    $filename=basename($file_name,$ext);
                    $filename = str_replace(" ","_",$filename);
                    $newFileName=$filename.time().".".$ext;
                    move_uploaded_file($file_tmp=$_FILES["files"]["tmp_name"][$key],"images/photo_gallery/properties/".$newFileName);
                
                    if($new_images != ""){
                        $new_images .= ";";
                    }
                    $new_images .= $newFileName;                
                } else {
                    array_push($error,"$file_name, format is invalid. Only .jpeg, .jpg and .png files allowed!");
                }
            }
        }

        if(isset($data['images'])){
            $images = $data['images'];
            if(count($images) > 0){
                foreach ($images as $key => $value) {
                    if (strpos($old_images, $value) !== false) {
                        if($new_images != ""){
                            $new_images .= ";";
                        }
                        $new_images .= $value;
                    }        
                }
            }    
        }

        $property->images = $new_images;

        if(empty($error)) $property->update();
        else return redirect()->back()->withErrors(['image', $error])->with('property',$property);

        return redirect('property/'.$property->id);
    }

    public function list($user_id)
    {
        $properties = DB::table('properties')
            ->join('users', 'properties.user_id', '=', 'users.id')
            ->leftJoin('countries', 'properties.country_id', '=', 'countries.id')
            ->leftJoin('property_types', 'properties.type', '=', 'property_types.id')
            ->select('countries.name as country_name', 'properties.*', 'property_types.name as type_name')
            ->where('properties.user_id', $user_id)
            ->orderBy('id', 'desc')
            ->get();

        return view('properties/list')->with('properties', $properties);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
