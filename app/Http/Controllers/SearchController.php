<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Country;
use App\Models\ServiceProviderType;
use App\Models\PropertyType;
use App\Models\Profile;
use App\Models\Group;
use App\User;
use Illuminate\Support\Facades\DB;

class SearchController extends Controller
{
    public function search_property(){

		$countries = Country::all();
        $property_types = PropertyType::all();

        $data = array('country' => null, 'category' => null, 'price' => null);
        $results = $this->searchProperties($data);

    	return view('search')
    	->with('countries', $countries)
    	->with('property_types', $property_types)
        ->with('results', $results)
    	->with('type', 'property');
    }

    public function search_group(){

		$countries = Country::all();
        $property_types = PropertyType::all();

        $data = array('country' => null, 'category' => null, 'use_of_property' => null, 'contract' => null);
        $results = $this->searchGroups($data);
    
    	return view('search')
        ->with('results', $results)
    	->with('countries', $countries)
        ->with('property_types', $property_types)
    	->with('type', 'group');
    }

    public function search_provider(){

		$countries = Country::all();
        $types = ServiceProviderType::all();

        $data = array('country' => null, 'company_name' => null, 'sp_type' => null);
        $results = $this->searchProviders($data);

    	return view('search')
        	->with('countries', $countries)
            ->with('types', $types)
            ->with('results', $results)
        	->with('type', 'provider');
    }

    private function searchProperties($data){

        $query = DB::table('properties')
            ->join('users', 'properties.user_id', '=', 'users.id')
            ->leftJoin('countries', 'properties.country_id', '=', 'countries.id');
        $query->select('users.first_name', 
                       'users.last_name',
                       'properties.country_id',
                       'title',
                       'price',
                       'properties.type',
                       'properties.created_at',
                       'properties.number_of_bedrooms',
                       'properties.total_land',
                       'name as country',
                       'images');

        if(!is_null($data['country'])){
            $query = $query->where('properties.country_id', $data['country']);
        }

        if(!is_null($data['category'])){
            $query = $query->where('properties.type', $data['category']);
        }

        if(isset($data['room']) && !is_null($data['room'])){
            $bedroom = $data['room'];
            if(in_array(1, $bedroom)){
                $query = $query->where('properties.number_of_bedrooms', 1);
            }else if(in_array(2, $bedroom)){
                $query = $query->where('properties.number_of_bedrooms', 2);
            }else if(in_array(3, $bedroom)){
                $query = $query->where('properties.number_of_bedrooms', 3);
            }else if(in_array(4, $bedroom)){
                $query = $query->where('properties.number_of_bedrooms', '>=', 4);
            }
        }

        if(isset($data['price']) && !is_null($data['price'])){  
            $range = $data['price'];
            $start = $range * 1000;
            $end = 1000 * 1000;        
            $range_arr = [$start, $end];
            $query = $query->whereBetween('price', $range_arr);
        }
        
        $results = $query->orderBy('created_at', 'desc')->get()->all();
        for($i = 0; $i < count($results); $i++) {
            $images_arr = explode(';',$results[$i]->images);
            if(isset($images_arr[0]) && $images_arr[0] != ""){
                $results[$i]->image = $images_arr[0];    
            }else{
                $results[$i]->image = null;
            }
            $results[$i]->url_title = str_replace(' ', '-', $results[$i]->title);
        }         
        return $results;
    }

    private function searchGroups($data){

        $query = DB::table('groups')
            ->join('users', 'groups.user_id', '=', 'users.id')
            ->leftJoin('countries', 'groups.country_id', '=', 'countries.id')
            ->leftJoin('property_types', 'groups.type_id', '=', 'property_types.id');
        $query->select('users.first_name', 
                       'users.last_name',
                       'groups.country_id',
                       'groups.name as group_name',
                       'groups.created_at',
                       'groups.contract',
                       'groups.type_id',
                       'groups.use_of_property',
                       'property_types.name as category_name',
                       'countries.name as country');
        
        if(!is_null($data['country'])){
            $query = $query->where('groups.country_id', $data['country']);
        }
        if(!is_null($data['category'])){
            $query = $query->where('groups.type_id', $data['category']);
        }
        if(!is_null($data['use_of_property'])){
            $query = $query->where('groups.use_of_property', $data['use_of_property']);
        }
        if(!is_null($data['contract'])){
            $query = $query->where('groups.contract', $data['contract']);
        }

        $results = $query->orderBy('created_at', 'desc')->get()->all();

        for($i = 0; $i < count($results); $i++) {
            $results[$i]->use_of_property = ucfirst(implode(' ', explode('_', $results[$i]->use_of_property)));
            $results[$i]->url_name = str_replace(' ', '-', $results[$i]->group_name);           
        }    
        
        return $results;
    }

    private function searchProviders($data){

        $query = DB::table('profiles')
            ->join('users', 'profiles.user_id', '=', 'users.id')
            ->leftJoin('countries', 'profiles.country_id', '=', 'countries.id');
        if(!is_null($data['sp_type'])){
            $query = $query->leftJoin('profile_service_provider_type', 'profiles.id', '=', 'profile_service_provider_type.profile_id');
        } 
        $query->select('users.first_name', 
                       'users.last_name',
                       'users.type',
                       'countries.name as country_name',
                       'profiles.description',
                       'profiles.company')
              ->where('users.type', 4);

        if(!is_null($data['country'])){
            $query = $query->where('profiles.country_id', $data['country']);
        }
         
        if(!is_null($data['company_name'])){
            $query = $query->where('profiles.company', 'LIKE', '%'.$data['company_name'].'%');
        }

        if(!is_null($data['sp_type'])){
            $query = $query->where('service_provider_type_id', $data['sp_type']);
        }
        
        $results = $query->get()->all();   

        for($i = 0; $i < count($results); $i++) {
            $results[$i]->url_name = str_replace(' ', '-', $results[$i]->first_name).'-'.str_replace(' ', '-', $results[$i]->last_name);           
        }    

        return $results;
    }

    public function search_results(Request $request){

        $data = $request->all();    
        $countries = Country::all();

        $service_provider_types = [];

        if($data['type'] == 'property')
        {
            $property_types = PropertyType::all();
            $results = $this->searchProperties($data);

            $price = isset($data['price']) ? $data['price'] : 0;
            $room = isset($data['room']) ? $data['room'] : null;

            return view('search')
                ->with('countries', $countries)
                ->with('type', $data['type'])
                ->with('property_types', $property_types)
                ->with('results', $results)
                ->with('category', $data['category'])
                ->with('price', $price)
                ->with('room', $room)
                ->with('country', $data['country']);
        }
        else if($data['type'] == 'group')
        {
            $property_types = PropertyType::all();
            $countries = Country::all();
            $results = $this->searchGroups($data);

            return view('search')
                ->with('countries', $countries)
                ->with('type', $data['type'])
                ->with('property_types', $property_types)
                ->with('results', $results)
                ->with('category', $data['category'])
                ->with('contract', $data['contract'])
                ->with('use_of_property', $data['use_of_property'])
                ->with('country', $data['country']);
        }
        else
        {
            $results = $this->searchProviders($data);
            $service_provider_types = ServiceProviderType::all();

            return view('search')
                ->with('countries', $countries)
                ->with('type', $data['type'])
                ->with('types', $service_provider_types)
                ->with('results', $results)
                ->with('sp_type', $data['sp_type'])
                ->with('company_name', $data['company_name'])
                ->with('country', $data['country']);
        }

        return view('search')
            ->with('countries', $countries)
            ->with('type', $data['type'])
            ->with('results', $results);
    }

    public function single_property($title){

        $title = str_replace('-', ' ', $title);

        $property = DB::table('properties')
            ->join('users', 'properties.user_id', '=', 'users.id')
            ->join('profiles', 'users.id', '=', 'profiles.user_id')
            ->leftJoin('countries', 'properties.country_id', '=', 'countries.id')
            ->leftJoin('property_types', 'properties.type', '=', 'property_types.id')
            ->select('countries.name as country_name',
                     'properties.*', 
                     'property_types.name as type_name',
                     'users.first_name', 'users.last_name', 'users.email',
                     'profiles.number', 'profiles.company', 'profiles.company_address')
            ->where('properties.title', $title)
            ->get()->first();

        $property->images = explode(';',$property->images);
        $property->situation = implode(' ', explode('_',$property->situation));
        return view('properties.single_property')->with('property', $property);
    }   

    public function single_group($name){

        $name = str_replace('-', ' ', $name);

        $group = DB::table('groups')
            ->join('users', 'groups.user_id', '=', 'users.id')
            ->join('profiles', 'users.id', '=', 'profiles.user_id')
            ->leftJoin('countries', 'groups.country_id', '=', 'countries.id')
            ->leftJoin('property_types', 'groups.type_id', '=', 'property_types.id')
            ->select('countries.name as country_name',
                     'groups.*', 
                     'property_types.name as type_name',
                     'users.first_name', 'users.last_name', 'users.email',
                     'profiles.number', 'profiles.company', 'profiles.company_address')
            ->where('groups.name', $name)
            ->get()->first();

        $group->capital_per_member = implode(' ', explode('_',$group->capital_per_member));
        $group->condition = implode(' ', explode('_',$group->condition));
        $group->location_type = implode(' ', explode('_',$group->location_type));
        $group->use_of_property = implode(' ', explode('_',$group->use_of_property));
        $group->total_capital_requirements = implode(' ', explode('_',$group->total_capital_requirements));

        $g = Group::find($group->id);
        $nationalities = $g->nationalities()->get()->toArray();
        $professions = $g->professions()->get()->toArray();
        $languages = $g->languages()->get()->toArray();
        $religions = $g->religions()->get()->toArray();

        return view('groups.single_group')
            ->with('nationalities', $nationalities)
            ->with('professions', $professions)
            ->with('languages', $languages)
            ->with('religions', $religions)
            ->with('group', $group);
    }   
}
