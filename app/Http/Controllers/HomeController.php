<?php

namespace App\Http\Controllers;

// use Illuminate\Http\Request;
use App\Models\Country;
use App\Models\PropertyType;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }

    public function new_ad()
    {
        $countries = Country::all(); // got this from database model
        $property_types = PropertyType::all(); 

        return view('listing_property')->with('countries', $countries)->with('property_types', $property_types);
    }
}
