<?php

use Illuminate\Database\Seeder;
use App\Models\ServiceProviderType;

class ServiceProviderTypesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         $types = array(
         	'Law Firms',
			'Corporate Service Providers',
			'Notaries',
			'Experts',
			'Land Surveyors',
			'Electricians',
			'Plumbers',
			'Engineers',
			'Masons',
			'Roofers',
			'Carpenters',
			'Cleaners',
			'Landscapers',
			'Pool Builders/Suppliers',
			'Architects',
			'Interior Designers',
			'Caterers',
			'Property Managers',
			'Lending Institutions',
			'Drivers/Taxis',
			'Security Services',
			'Other'
        );

        foreach ($types as $key => $value) {
            $type = ServiceProviderType::updateOrCreate([
                'name' => $value
            ], [
                'name' => $value
            ]);
        }
    }
}
