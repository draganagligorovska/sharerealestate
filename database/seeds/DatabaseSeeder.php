<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);

    	$this->call(CountriesSeeder::class);
        $this->call(LanguagesTableSeeder::class);
        $this->call(NationalitiesTableSeeder::class);
        $this->call(ProfessionsTableSeeder::class);
        $this->call(PropertyTypeTableSeeder::class);
        $this->call(ReligiousTableSeeder::class);
        $this->call(ServiceProviderTypesSeeder::class);
    }
}
