<?php

use Illuminate\Database\Seeder;
use App\Models\Nationality;
use Illuminate\Support\Facades\Config;

class NationalitiesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $nationalities = Config::get("enums.nationalities");
        
        foreach ($nationalities as $nationality) {
            $religion = Nationality::updateOrCreate([
                'name' => $nationality
            ]);
        }
    }
}
