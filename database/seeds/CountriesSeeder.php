<?php

use App\Models\Country;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Config;

class CountriesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $countries = Config::get("enums.countries");

        foreach ($countries as $code => $country) {
            $country = Country::updateOrCreate([
                'code_alpha_2' => $code
            ], [
                'code_alpha_2' => $code,
                'name'         => $country
            ]);
        }
    }
}
