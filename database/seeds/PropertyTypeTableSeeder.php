<?php

use App\Models\PropertyType;
use Illuminate\Database\Seeder;

class PropertyTypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         $types = array(
         	'House', 
            'Apartment', 
            'Villa',
            'Chalet',
            'Chateau', 
            'Parking', 
            'Land',
            'Office', 
            'Commercial', 
            'Industrial'
        );
        foreach ($types as $key => $value) {
            $country = PropertyType::updateOrCreate([
                'name' => $value
            ], [
                'name' => $value
            ]);
        }
    }
}
