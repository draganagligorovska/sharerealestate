<?php

use Illuminate\Database\Seeder;
use App\Models\Religion;
use Illuminate\Support\Facades\Config;

class ReligiousTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $religious = Config::get("enums.religious");

        foreach ($religious as $code => $religion) {
            $religion = Religion::updateOrCreate([
                'name' => $religion
            ], [
                'name' => $religion
            ]);
        }
    }
}
