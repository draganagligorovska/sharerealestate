<?php

use Illuminate\Database\Seeder;
use App\Models\Language;
use Illuminate\Support\Facades\Config;

class LanguagesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $languages = Config::get("enums.languages");

        foreach ($languages as $code => $language) {
            $religion = Language::updateOrCreate([
                'code' => $code
            ], [
                'code' => $code,
                'name' => $language
            ]);
        }
    }
}
