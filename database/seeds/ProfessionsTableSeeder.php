<?php

use Illuminate\Database\Seeder;
use App\Models\Profession;
use Illuminate\Support\Facades\Config;

class ProfessionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $professions = Config::get("enums.professions");

        foreach ($professions as $profession) {
            $religion = Profession::updateOrCreate([
                'name' => $profession
            ], [
                'name' => $profession
            ]);
        }
    }
}
