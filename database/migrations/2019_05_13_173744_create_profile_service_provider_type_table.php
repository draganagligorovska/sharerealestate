<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProfileServiceProviderTypeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('profile_service_provider_type', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->bigInteger('profile_id')->unsigned();
            $table->foreign('profile_id')->references('id')->on('profiles')->onDelete('CASCADE')->onUpdate('CASCADE');

            $table->bigInteger('service_provider_type_id')->unsigned();
            $table->foreign('service_provider_type_id')->references('id')->on('service_provider_types')->onDelete('CASCADE')->onUpdate('CASCADE');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('profile_service_provider_type');
    }
}
