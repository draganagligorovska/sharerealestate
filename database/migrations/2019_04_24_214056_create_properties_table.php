<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePropertiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('properties', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('title');
            $table->longText('description');
            $table->string('image');
            $table->string('price');
            $table->integer('type');
            
            $table->unsignedInteger('country_id');
            $table->foreign('country_id')->references('id')->on('countries')->onDelete('CASCADE')->onUpdate('CASCADE');
            
            $table->string('address');
            $table->string('year_of_construction');
            $table->integer('number_of_bedrooms');
            $table->string('total_land');
            $table->string('living_area');
            $table->string('situation');
            
            $table->boolean('balcony')->default(false);
            $table->boolean('pool')->default(false);
            $table->boolean('parking')->default(false);
            $table->boolean('central_heating')->default(false);
            $table->boolean('gas')->default(false);

            $table->string('contact_name');
            $table->string('contact_telephone');
            $table->string('contact_email');

            $table->integer('promotion_period_years');
            $table->integer('promotion_period_months');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('properties');
    }
}
