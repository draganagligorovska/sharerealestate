<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGroupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('groups', function (Blueprint $table) {

            $table->bigIncrements('id');
            $table->string('name');
            $table->longText('description');
            $table->string('region')->nullable();
            $table->unsignedInteger('country_id');
            $table->foreign('country_id')->references('id')->on('countries');
            $table->unsignedInteger('type_id');
            $table->foreign('type_id')->references('id')->on('property_types');
            $table->string('capital_per_member');
            $table->integer('num_of_bedrooms')->nullable();

            $table->boolean('balcony')->default(false);
            $table->boolean('pool')->default(false);
            $table->boolean('parking')->default(false);
            $table->boolean('central_heating')->default(false);
            $table->boolean('gas')->default(false);

            $table->string('location_type')->nullable();
            $table->string('use_of_property');
            $table->string('condition')->nullable();
            
            $table->integer('year_of_construction_from')->nullable();
            $table->integer('year_of_construction_to')->nullable();

            $table->string('total_land_from')->nullable();
            $table->string('total_land_to')->nullable();

            $table->string('living_area_from')->nullable();
            $table->string('living_area_to')->nullable();

            $table->integer('max_num_of_members')->nullable();
            $table->string('total_capital_requirements')->nullable();

            $table->boolean('gay_friendly')->default(false);
            $table->boolean('smoker_tolerant')->default(false);
            $table->boolean('alcohol_tolerant')->default(false);
            $table->boolean('pet_friendly')->default(false);
            $table->boolean('family_friendly')->default(false);
            $table->boolean('disability_accommodating')->default(false);

            $table->integer('age_from')->nullable();
            $table->integer('age_to')->nullable();

            $table->string('living_with')->nullable();
            $table->string('sexual_orientation')->nullable();
           
            $table->string('contract');

            $table->integer('user_id');
           
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('groups');
    }
}
